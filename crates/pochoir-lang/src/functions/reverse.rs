//! Reverse the order of characters of a [`Value::String`] or the order of elements in a
//! [`Value::Array`].
//!
//! ### Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">reverse</span>(<span class="string">"123456789"</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="string">"987654321"</span></code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="position: relative; margin-top: 2rem; border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">reverse</span>([<span class="string">"a"</span>, <span class="string">"b"</span>, <span class="string">"c"</span>])</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code>[<span class="string">"c"</span>, <span class="string">"b"</span>, <span class="string">"a"</span>]</code></pre></div>
use crate::{FunctionResult, Value};

pub(crate) fn reverse(val: Value) -> FunctionResult<Value> {
    if let Value::String(val) = val {
        Ok(Value::String(val.chars().rev().collect()))
    } else if let Value::Array(mut val) = val {
        val.reverse();
        Ok(Value::Array(val))
    } else {
        Err(format!(
            "mismatched types for arguments of the `reverse` function: expected String or Array, found {}",
            val.type_name()
        ).into())
    }
}

#[cfg(test)]
mod tests {
    use crate::IntoValue;

    use super::*;

    #[test]
    fn reverse_test() {
        assert_eq!(
            reverse("foo bar".into_value()).unwrap(),
            "rab oof".into_value(),
        );

        assert_eq!(
            reverse(vec!["a", "b", "c"].into_value()).unwrap(),
            vec!["c", "b", "a"].into_value(),
        );
    }
}
