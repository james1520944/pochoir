//! Returns `true` if the string starts with the provided pattern.
//!
//! ### Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">starts_with</span>(<span class="string">"Hello world"</span>, <span class="string">"Hello "</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="bool-val">true</span></code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">starts_with</span>(<span class="string">"Hello world"</span>, <span class="string">"world"</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="bool-val">false</span></code></pre></div>
use crate::FunctionResult;

pub(crate) fn starts_with(val: String, pattern: String) -> FunctionResult<bool> {
    Ok(val.starts_with(&pattern))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn starts_with_test() {
        assert!(starts_with("Hello world".to_string(), "Hello ".to_string()).unwrap());
        assert!(!starts_with("Hello world".to_string(), "world".to_string()).unwrap());
    }
}
