//! Convert any value to a string.
//!
//! ### Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">to_string</span>(<span class="number">42.12</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="string">"42.12"</span></code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="position: relative; margin-top: 2rem; border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">to_string</span>([<span class="string">"a"</span>, null, <span class="number">42</span>])</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="string">"[a, null, 42]"</span></code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="position: relative; margin-top: 2rem; border-left: 2px solid green;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">to_string</span>({ key: <span class="string">"value"</span>, <span class="string">"hello"</span>: <span class="number">42</span>, <span class="string">"foo bar"</span>: <span class="bool-val">true</span> })</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid green;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="string">"{ key: value, hello: 42, \"foo bar\": true }"</span></code></pre></div>
use crate::{FunctionResult, Value};

pub(crate) fn to_string(val: Value) -> FunctionResult<String> {
    Ok(val.to_string())
}

#[cfg(test)]
mod tests {
    use crate::{object, IntoValue};

    use super::*;

    #[test]
    fn to_string_test() {
        assert_eq!(to_string(42.12.into_value()).unwrap(), "42.12".to_string(),);

        assert_eq!(
            to_string("hello".into_value()).unwrap(),
            "hello".to_string(),
        );

        assert_eq!(to_string(true.into_value()).unwrap(), "true".to_string(),);

        assert_eq!(to_string(Value::Null).unwrap(), "null".to_string(),);

        assert_eq!(
            to_string(vec!["a".into_value(), Value::Null, Value::Number(42.0)].into_value())
                .unwrap(),
            "[a, null, 42]".to_string(),
        );

        assert_eq!(
            to_string(
                object! {
                    "k" => "v".into_value(),
                    "hello" => Value::Number(42.0),
                    "foo bar" => true.into_value(),
                }
                .into_value()
            )
            .unwrap(),
            "{k: v, hello: 42, \"foo bar\": true}".to_string(),
        );
    }
}
