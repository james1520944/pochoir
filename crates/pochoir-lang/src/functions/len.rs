//! Get the number of characters in a string (don't forget that unicode code points can contain
//! several characters), the number of elements in an array or the number of key/value pairs of an
//! object.
//!
//! ### Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">len</span>(<span class="string">"hello world"</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="number">11</span></code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="position: relative; margin-top: 2rem; border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">len</span>([<span class="number">1</span>, <span class="number">2</span>, <span class="number">3</span>])</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="number">3</span></code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="position: relative; margin-top: 2rem; border-left: 2px solid green;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">len</span>({ name: <span class="string">"Georges"</span>, age: <span class="number">19</span>, hobbies: [<span class="string">"swimming"</span>, <span class="string">"video games"</span>] })</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid green;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="number">3</span></code></pre></div>
use crate::{FunctionResult, Value};

pub(crate) fn len(val: Value) -> FunctionResult<usize> {
    if let Value::String(val) = val {
        Ok(val.len())
    } else if let Value::Array(val) = val {
        Ok(val.len())
    } else if let Value::Object(val) = val {
        Ok(val.len())
    } else {
        Err(format!(
            "mismatched types for arguments of the `len` function: expected String or Array, found {}",
            val.type_name()
        ).into())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{object, IntoValue};

    #[test]
    fn len_test() {
        assert_eq!(len("foo".into_value()).unwrap(), 3);

        assert_eq!(
            len(vec![Value::Number(1.0), Value::Number(2.0)].into_value()).unwrap(),
            2,
        );

        assert_eq!(
            len(object! {
                "name" => "Georges".into_value(),
                "age" => 19.into_value(),
                "hobies" => vec![Value::String("swimming".to_string()), "video games".into_value()].into_value(),
            }.into_value()).unwrap(),
            3,
        );
    }
}
