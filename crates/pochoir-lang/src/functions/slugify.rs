//! Converts a value into a slug, usable in URLs or file names.
//!
//! ### Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">slugify</span>(<span class="string">"Hello world!"</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code><span class="string">"hello-world"</span></code></pre></div>
use crate::FunctionResult;

pub(crate) fn slugify(val: String) -> FunctionResult<String> {
    Ok(slug::slugify(val))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn slugify_test() {
        assert_eq!(
            slugify("Hello world!".into()).unwrap(),
            "hello-world".to_string(),
        );
    }
}
