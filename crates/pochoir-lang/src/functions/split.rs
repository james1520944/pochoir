//! Split a string into an array based on the provided separator.
//!
//! ### Example
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">split</span>(<span class="string">"The little fox"</span>, <span class="string">" "</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid red;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code>[<span class="string">"The"</span>, <span class="string">"little"</span>, <span class="string">"fox"</span>]</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="position: relative; margin-top: 2rem; border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">input</span><code><span class="fn">split</span>(<span class="string">"4;3;4;;1;6"</span>, <span class="string">";"</span>)</code></pre></div>
//!
//! <div class="example-wrap"><pre class="rust rust-example-rendered" style="border-left: 2px solid blue;"><span style="position: absolute; right: 0; top: 0; padding: 0.1rem 0.4rem 0 0; font-size: 0.75em; font-weight: bold; color: #333;">output</span><code>[<span class="string">"4"</span>, <span class="string">"3"</span>, <span class="string">"4"</span>, <span class="string">""</span>, <span class="string">"1"</span>, <span class="string">"6"</span>]</code></pre></div>
use crate::{FunctionResult, IntoValue, Value};

pub(crate) fn split(val: String, separator: String) -> FunctionResult<Vec<Value>> {
    Ok(val.split(&separator).map(IntoValue::into_value).collect())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn split_test() {
        assert_eq!(
            split("The little fox".to_string(), " ".to_string()).unwrap(),
            vec![
                "The".into_value(),
                "little".into_value(),
                "fox".into_value()
            ]
        );

        assert_eq!(
            split("4;3;4;;1;6".to_string(), ";".to_string(),).unwrap(),
            vec![
                "4".into_value(),
                "3".into_value(),
                "4".into_value(),
                "".into_value(),
                "1".into_value(),
                "6".into_value()
            ]
        );
    }
}
