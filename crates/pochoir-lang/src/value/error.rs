use serde::{de, ser};
use std::{fmt, string::ToString};
use thiserror::Error;

/// Shorthand for [`Result`] type.
///
/// [`Result`]: std::result::Result
pub type Result<T> = std::result::Result<T, Error>;

#[derive(Error, Debug)]
pub enum Error {
    #[error("error when deserializing: {0}")]
    Deserialize(String),

    #[error("error when serializing: {0}")]
    Serialize(String),
}

impl de::Error for Error {
    fn custom<T: fmt::Display>(msg: T) -> Self {
        Self::Deserialize(msg.to_string())
    }

    fn invalid_type(unexp: de::Unexpected, exp: &dyn de::Expected) -> Self {
        if let de::Unexpected::Unit = unexp {
            Self::custom(format_args!("invalid type: null, expected {exp}"))
        } else {
            Self::custom(format_args!("invalid type: {unexp}, expected {exp}"))
        }
    }
}

impl ser::Error for Error {
    fn custom<T: fmt::Display>(msg: T) -> Self {
        Self::Serialize(msg.to_string())
    }
}
