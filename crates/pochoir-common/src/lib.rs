#![doc(html_logo_url = "https://gitlab.com/encre-org/pochoir/raw/main/.assets/logo.png")]
#![forbid(unsafe_code)]
#![warn(
    missing_debug_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unstable_features,
    unused_import_braces,
    unused_qualifications,
    rustdoc::private_doc_tests,
    rustdoc::broken_intra_doc_links,
    rustdoc::private_intra_doc_links,
    clippy::unnecessary_wraps,
    clippy::too_many_lines,
    clippy::string_to_string,
    clippy::explicit_iter_loop,
    clippy::unnecessary_cast,
    clippy::missing_errors_doc,
    clippy::pedantic,
    clippy::clone_on_ref_ptr,
    clippy::non_ascii_literal,
    clippy::dbg_macro,
    clippy::map_err_ignore,
    clippy::use_debug,
    clippy::map_err_ignore,
    clippy::use_self,
    clippy::useless_let_if_seq,
    clippy::verbose_file_reads,
    clippy::panic,
    clippy::unimplemented,
    clippy::todo
)]
#![allow(clippy::module_name_repetitions, clippy::must_use_candidate)]
pub mod error;
pub mod span;
pub mod stream_parser;

pub use error::{Error, Result};
pub use span::Spanned;
pub use stream_parser::StreamParser;
