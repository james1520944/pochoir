use axum::{
    body::Body,
    http::{header::CONTENT_TYPE, StatusCode, Uri},
    response::{
        sse::{Event, KeepAlive},
        Response, Sse,
    },
    routing::get,
    Router,
};
use clap::{Parser, Subcommand};
use notify_debouncer_mini::{new_debouncer, notify::RecursiveMode, DebounceEventResult};
use pochoir::{common::Spanned, component_not_found, lang::Context, ComponentFile};
use std::{borrow::Cow, convert::Infallible, env, fs, path::Path, time::Duration};
use tokio::sync::broadcast::{self, Sender};

// TODO: Avoid reloading the complete page but just dynamically patch the DOM
const LIVERELOAD_SCRIPT: &str = r#"<script>
  let wasDisconnected = false;

  const connect = () => {
    const socket = new EventSource("/__livereload");

    socket.addEventListener("reload", (event) => {
      console.log("[LIVERELOAD] Reload message received. Reloading the page…");
      location.reload(true);
    });

    socket.addEventListener("open", () => {
      console.log("[LIVERELOAD] Connected.");

      if (wasDisconnected) {
        location.reload(true);
      }
    });

    socket.addEventListener("error", (e) => {
      console.log("[LIVERELOAD] Disconnected.");
      wasDisconnected = true;
      setTimeout(connect, 5000);
    });
  };

  connect();
</script>"#;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// Change the build directory.
    #[arg(short, long, default_value = "build")]
    build_dir: String,

    /// Watch the HTML files in the current directory.
    #[arg(short, long)]
    watch: bool,

    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand, PartialEq)]
enum Command {
    Serve {
        /// Port on which the development server will listen.
        #[arg(short, long, default_value = "3000")]
        port: usize,
    },
    Build,
}

fn html_error_page(error: &Spanned<pochoir::Error>, source: &str) -> String {
    format!(
        r#"<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <style>body {{ font-family: sans-serif; font-size: 1.4em; color: #333; padding: 2rem; margin: 0; }}</style>
        <title>Error</title>
    </head>
    <body>
        {}
    </body>
</html>"#,
        pochoir::error::display_html_error(error, source)
    )
}

fn compile_file(file: &str, build_dir: &str, context: &mut Context, prod: bool, watch: bool) {
    match pochoir::compile(file, context, |name| {
        let file_path = if name.ends_with(".html") {
            Cow::Borrowed(name)
        } else {
            Cow::Owned(format!("{name}.html"))
        };

        // TODO: Cache of read files
        if let Ok(file_content) = fs::read_to_string(&*file_path) {
            Ok(ComponentFile::new(
                Path::new(&*file_path).to_owned(),
                file_content,
            ))
        } else {
            Err(component_not_found(name))
        }
    }) {
        Ok(result) => {
            fs::write(Path::new(&build_dir).join(file), result)
                .expect("failed to write built file");
        }
        Err(error) => {
            let source = fs::read_to_string(error.file_path()).unwrap_or_else(|e| {
                panic!("failed to read file `{}`: {e}", error.file_path().display())
            });

            if prod {
                eprintln!("{}", pochoir::error::display_ansi_error(&error, &source));

                if !watch {
                    std::process::exit(101);
                }
            } else {
                eprintln!("{}", pochoir::error::display_ansi_error(&error, &source));
                fs::write(
                    Path::new(&build_dir).join(file),
                    html_error_page(&error, &source),
                )
                .expect("failed to write built file");
            }
        }
    }
}

fn do_compile(build_dir: &str, prod: bool, watch: bool) {
    // TODO: Fetch from file
    let mut context = Context::new();

    // Compile all HTML files in current directory
    for file in fs::read_dir(".")
        .expect("failed to read files in the current directory")
        .filter_map(|f| {
            if let Ok(f) = f {
                if f.path().is_file() {
                    return Some(f);
                }
            }
            None
        })
    {
        if file.path().extension().map_or(false, |e| e == "html") {
            compile_file(
                file.path()
                    .to_str()
                    .expect("failed to convert path to string"),
                build_dir,
                &mut context,
                prod,
                watch,
            )
        } else {
            fs::copy(file.path(), Path::new(build_dir).join(file.path()))
                .expect("failed to copy regular file");
        }
    }
}

#[tokio::main]
async fn launch_server(build_dir: String, port: usize, reload_tx: Option<Sender<()>>) {
    let is_watching = reload_tx.is_some();
    let static_files = move |uri: Uri| async move {
        let path = Path::new(&build_dir).join(if uri.path() == "/" {
            "index.html"
        } else {
            uri.path().trim_start_matches('/')
        });

        if let Ok(mut file) = tokio::fs::read(&path).await {
            let mime = mime_guess::from_path(path)
                .first_raw()
                .unwrap_or("application/octet-stream");

            if mime.contains("html") && is_watching {
                let mut body = String::from_utf8(file).expect("failed to read HTML file to utf8");
                let start_len = body.len();

                // Try to append it to the body
                body = body.replacen("</body>", &format!("{LIVERELOAD_SCRIPT}</body>"), 1);

                if body.len() == start_len {
                    // Try to append it to the head
                    body = body.replacen("</head>", &format!("{LIVERELOAD_SCRIPT}</head>"), 1);

                    if body.len() == start_len {
                        // Try to append it to the file
                        body.push_str(LIVERELOAD_SCRIPT);
                    }
                }

                file = body.as_bytes().to_vec();
            }

            Response::builder()
                .header(CONTENT_TYPE, mime)
                .body(Body::from(file))
                .expect("response should be valid")
        } else {
            Response::builder()
                .status(StatusCode::NOT_FOUND)
                .header(CONTENT_TYPE, "text/html")
                .body(Body::from("404 Not found!"))
                .expect("response should be valid")
        }
    };

    let mut app = Router::new().fallback(static_files);

    if let Some(reload_tx) = reload_tx {
        let livereload_route = || async move {
            let stream = async_stream::stream! {
                if reload_tx.subscribe().recv().await == Ok(()) {
                    yield Ok::<Event, Infallible>(Event::default().data("reload"));
                }
            };

            Sse::new(stream).keep_alive(KeepAlive::default())
        };

        app = app.route("/__livereload", get(livereload_route));
    }

    println!("🌐 Server started at http://localhost:{port}");
    open::that(format!("http://localhost:{port}")).expect("failed to open server URL in browser");

    axum::Server::bind(&format!("0.0.0.0:{port}").parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

fn main() {
    let cli = Cli::parse();

    let build_dir_path = Path::new(&cli.build_dir);
    let prod = cli.command == Command::Build;

    if build_dir_path.exists() {
        fs::remove_dir_all(build_dir_path).expect("failed to remove build directory");
    }

    fs::create_dir(&cli.build_dir).expect("failed to create build directory");

    let build_dir_path = build_dir_path
        .canonicalize()
        .expect("failed to canonicalize build directory");
    let build_dir = cli.build_dir.clone();

    let (reload_tx, _debouncer) = if cli.watch {
        // First build
        println!("🔨 Building for the first time");
        do_compile(&cli.build_dir, prod, true);

        let (reload_tx, _) = broadcast::channel(8);

        let mut debouncer = {
            let reload_tx = reload_tx.clone();
            new_debouncer(
                Duration::from_millis(100),
                None,
                move |result: DebounceEventResult| match result {
                    Ok(events) => {
                        if events.iter().any(|e| {
                            !e.path
                                .canonicalize()
                                .map_or(false, |p| p.starts_with(&build_dir_path))
                        }) {
                            // Ignore errors when clearing the screen
                            let _ = clearscreen::clear();
                            println!("♻️  File update, rebuilding");
                            do_compile(&build_dir, prod, true);
                            let _ = reload_tx.send(());
                        }
                    }
                    Err(e) => eprintln!("watch error: {:?}", e),
                },
            )
            .expect("failed to start the file watcher")
        };

        let current_dir = env::current_dir().expect("failed to get path of current directory");
        debouncer
            .watcher()
            .watch(&current_dir, RecursiveMode::Recursive)
            .expect("failed to watch current directory for changes");
        println!("🔎 Watching for changes in {}", current_dir.display());

        (Some(reload_tx), Some(debouncer))
    } else {
        do_compile(&cli.build_dir, prod, false);
        (None, None)
    };

    match cli.command {
        Command::Serve { port } => {
            launch_server(cli.build_dir, port, reload_tx);
        }
        Command::Build => {
            if cli.watch {
                loop {
                    std::thread::sleep(Duration::from_millis(100));
                }
            }
        }
    }
}
