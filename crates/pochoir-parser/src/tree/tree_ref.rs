use pochoir_common::Spanned;
use pochoir_template_engine::{Escaping, TemplateBlock};
use selectors::{matching, parser::SelectorList};
use std::{borrow::Cow, fmt};

use super::{build_selector_list, selection::InnerSelector, Tree, TreeNode, TreeRefId};
use crate::{Node, OwnedTree, ParsedNode, Result};

/// A reference to a node in the tree.
#[derive(Clone, Copy)]
pub struct TreeRef<'a, 'b> {
    /// A unique ID identifying the node in the tree.
    pub id: TreeRefId,

    /// The tree owning this node.
    pub tree: &'b Tree<'a>,
}

impl<'a, 'b> TreeRef<'a, 'b> {
    #[allow(clippy::panic)]
    fn inner(&self) -> &TreeNode<'a> {
        self.tree.nodes.get(&self.id).unwrap_or_else(|| {
            panic!(
                "failed to find the current node (id {:?}) in the tree",
                self.id
            )
        })
    }

    pub fn id(&self) -> TreeRefId {
        self.id
    }

    /// Get an immutable reference to the underlying [`ParsedNode`] containing the [`Node`] and the
    /// location of the span of text where it was defined.
    ///
    /// # Panics
    ///
    /// Panics if the node does not exist in the tree (it could have been removed).
    pub fn spanned_data(&self) -> &ParsedNode<'a> {
        &self.inner().data
    }

    /// Get an immutable reference to the underlying [`Node`].
    ///
    /// # Panics
    ///
    /// Panics if the node does not exist in the tree (it could have been removed).
    pub fn data(&self) -> &Node<'a> {
        &self.inner().data
    }

    /// Get a reference to the parent of this node.
    #[must_use]
    pub fn parent(&self) -> Self {
        Self {
            id: self.inner().parent,
            tree: self.tree,
        }
    }

    /// Search all parents of the node until it finds a node matching the provided CSS selector.
    ///
    /// Returns `None` if the selector is invalid or if a node matching the selector could not be
    /// found.
    ///
    /// This method is equivalent to the [`Element.closest`](https://developer.mozilla.org/en-US/docs/Web/API/Element/closest) method in Javascript.
    pub fn closest(&self, selector: &str) -> Option<Self> {
        let mut tree_ref = *self;
        let selector_list = build_selector_list(selector).ok()?;

        loop {
            if tree_ref.is_matching(&selector_list) {
                return Some(tree_ref);
            } else if tree_ref.id == TreeRefId::Root {
                return None;
            }

            tree_ref = tree_ref.parent();
        }
    }

    /// Get a reference to the previous sibling of this node.
    pub fn prev_sibling(&self) -> Option<Self> {
        let children = self.parent().children();
        let index = children.iter().position(|tree_ref| tree_ref.id == self.id);

        match index {
            Some(i) if i > 0 => Some(Self {
                id: children[i - 1].id,
                tree: self.tree,
            }),
            _ => None,
        }
    }

    /// Get a reference to the next sibling of this node.
    pub fn next_sibling(&self) -> Option<Self> {
        let children = self.parent().children();
        let index = children.iter().position(|tree_ref| tree_ref.id == self.id);

        match index {
            Some(i) if i + 1 < children.len() => Some(Self {
                id: children[i + 1].id,
                tree: self.tree,
            }),
            _ => None,
        }
    }

    /// Get a list of references of the children of this node.
    pub fn children(&self) -> Vec<Self> {
        self.inner()
            .children
            .iter()
            .map(|id| self.tree.get(*id))
            .collect()
    }

    /// Get a cloned list of IDs of this node children.
    pub fn children_id(&self) -> Vec<TreeRefId> {
        self.inner().children.clone()
    }

    /// Select a single node in the tree, relatively to this node, using a CSS selector.
    ///
    /// Returns `None` if the node is not found or if the selector is not a valid CSS selector.
    pub fn select(&self, selector: &str) -> Option<TreeRef> {
        let selector_list = build_selector_list(selector).ok()?;
        self.traverse_depth()
            .find(|tree_ref| tree_ref.is_matching(&selector_list))
    }

    /// Select several nodes in the tree, relatively to this node, using a CSS selector.
    ///
    /// Returns an empty `Vec` if the selector is not a valid CSS selector.
    pub fn select_all(&self, selector: &str) -> Vec<TreeRef> {
        if let Ok(selector_list) = build_selector_list(selector) {
            self.traverse_depth()
                .filter(|tree_ref| tree_ref.is_matching(&selector_list))
                .collect()
        } else {
            vec![]
        }
    }

    /// Get the element name of the [`TreeRef`].
    ///
    /// Returns `Some(name)` if it is a [`Node::Element`].
    ///
    /// Returns `None` otherwise.
    ///
    /// [`Node::Element`]: crate::Node::Element
    pub fn name(&self) -> Option<Cow<'a, str>> {
        if let Node::Element(ref name, _) = self.data() {
            Some(name.clone())
        } else {
            None
        }
    }

    /// Get the text of the [`TreeRef`].
    ///
    /// Returns the concatenated text content of child nodes if it is a [`Node::Element`].
    ///
    /// Returns the text content if it is a [`Node::TemplateBlock(TemplateBlock::RawText(_))`](`Node::TemplateBlock`).
    ///
    /// All template expressions and statements just return their raw content with no spaces inside
    /// their delimiters (will return `{{expr}}` even if it was `{{ expr }}`, `{!expr!}` event if
    /// it was `{! expr !}` and `{%stmt%}` even if it was `{% stmt %}`).
    ///
    /// [`Node::Element`]: crate::Node::Element
    /// [`Node::Text`]: crate::Node::Text
    pub fn text(&self) -> Cow<'a, str> {
        match self.data() {
            Node::Element(_, _) => {
                let children = self.children();
                Cow::Owned(children.into_iter().map(|child| child.text()).collect())
            }
            Node::TemplateBlock(block) => Cow::Owned(match block {
                TemplateBlock::RawText(t) => t.to_string(),
                TemplateBlock::Expr(t, is_escaped) => {
                    if *is_escaped {
                        format!("{{{{{t}}}}}")
                    } else {
                        format!("{{!{t}!}}")
                    }
                }
                TemplateBlock::Stmt(t) => format!("{{%{t}%}}"),
            }),
            _ => Cow::Borrowed(""),
        }
    }

    /// Get the value of an attribute belonging to a [`Node::Element`].
    ///
    /// Returns `None` if the attribute is not present or if this function is not called on a
    /// [`Node::Element`].
    ///
    /// Returns `Some("")` if the attribute is present but does not have a value (e.g. in `<input
    /// type="checkbox" checked>`, the `checked` HTML attribute is present but does not have a value).
    ///
    /// Returns `Some(value)` if the attribute is present and does have a value.
    ///
    /// All template expressions and statements just return their raw content with no spaces inside
    /// their delimiters (will return `{{expr}}` even if it was `{{ expr }}`, `{!expr!}` event if
    /// it was `{! expr !}` and `{%stmt%}` even if it was `{% stmt %}`).
    pub fn attr(&self, name: &str) -> Option<String> {
        if let Node::Element(_, ref attrs) = self.data() {
            Some(
                attrs
                    .get(Cow::Borrowed(name))?
                    .iter()
                    .map(|b| match &**b {
                        TemplateBlock::RawText(t) => t.to_string(),
                        TemplateBlock::Expr(t, is_escaped) => {
                            if *is_escaped {
                                format!("{{{{{t}}}}}")
                            } else {
                                format!("{{!{t}!}}")
                            }
                        }
                        TemplateBlock::Stmt(t) => format!("{{%{t}%}}"),
                    })
                    .collect::<String>(),
            )
        } else {
            None
        }
    }

    /// Get the value of an attribute belonging to a [`Node::Element`].
    ///
    /// Returns `None` if the attribute is not present or if this function is not called on a
    /// [`Node::Element`].
    ///
    /// Returns `Some("")` if the attribute is present but does not have a value (e.g. in `<input
    /// type="checkbox" checked>`, the `checked` HTML attribute is present but does not have a value).
    ///
    /// Returns `Some(value)` if the attribute is present and does have a value.
    ///
    /// Template expressions and statements can be returned.
    pub fn attr_spanned(&self, name: &str) -> Option<Vec<Spanned<TemplateBlock>>> {
        if let Node::Element(_, ref attrs) = self.data() {
            attrs.get(Cow::Borrowed(name)).cloned()
        } else {
            None
        }
    }

    pub fn sub_tree(&self) -> Tree<'a> {
        let mut tree = Tree::new(self.tree.file_path());

        // Prevent any ID collision
        tree.next_id = self.tree.next_id;

        for tree_ref in self.traverse_depth() {
            let parent_id = if tree_ref.parent().id() == self.id() {
                TreeRefId::Root
            } else {
                tree_ref.parent().id()
            };

            tree.insert_with_id(
                tree_ref.id(),
                parent_id,
                tree_ref.children_id(),
                tree_ref.spanned_data().clone(),
            );
        }
        tree
    }

    pub fn traverse_breadth(&self) -> impl Iterator<Item = TreeRef<'a, '_>> {
        let parent = self.tree.get(self.id);

        super::traverse::TraverseBreadthIter {
            children: parent.children(),
            index: 0,
        }
    }

    pub fn traverse_depth(&self) -> impl Iterator<Item = TreeRef<'a, '_>> {
        let mut queue = self.tree.get(self.id).children();
        queue.reverse();

        super::traverse::TraverseDepthIter { queue }
    }

    pub fn is_matching(&self, selector_list: &SelectorList<InnerSelector>) -> bool {
        let mut ctx = matching::MatchingContext::new(
            matching::MatchingMode::Normal,
            None,
            None,
            matching::QuirksMode::NoQuirks,
        );

        matching::matches_selector_list(selector_list, self, &mut ctx)
    }
}

impl<'a, 'b> PartialEq for TreeRef<'a, 'b> {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl<'a, 'b> fmt::Debug for TreeRef<'a, 'b> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("TreeRef")
            .field("id", &self.id)
            .field("__inner_node", &self.data())
            .finish()
    }
}

/// A mutable reference to a node in the tree.
pub struct TreeRefMut<'a, 'b> {
    /// A unique ID identifying the node in the tree.
    pub id: TreeRefId,

    /// The tree owning this node.
    pub tree: &'b mut Tree<'a>,
}

impl<'a, 'b> TreeRefMut<'a, 'b> {
    #[allow(clippy::panic)]
    fn inner(&self) -> &TreeNode<'a> {
        self.tree.nodes.get(&self.id).unwrap_or_else(|| {
            panic!(
                "failed to find the current node (id {:?}) in the tree (mutable)",
                self.id
            )
        })
    }

    #[allow(clippy::panic)]
    fn inner_mut(&mut self) -> &mut TreeNode<'a> {
        self.tree.nodes.get_mut(&self.id).unwrap_or_else(|| {
            panic!(
                "failed to find the current node (id {:?}) in the tree (mutable)",
                self.id
            )
        })
    }

    pub fn id(&self) -> TreeRefId {
        self.id
    }

    pub fn as_ref(&self) -> TreeRef<'a, '_> {
        TreeRef {
            id: self.id,
            tree: self.tree,
        }
    }

    /// Get a mutable reference to the underlying [`Node`].
    ///
    /// # Panics
    ///
    /// Panics if the node does not exist in the tree (it could have been removed).
    pub fn data(&mut self) -> &mut Node<'a> {
        &mut self.inner_mut().data
    }

    /// Get an mutable reference to the underlying [`ParsedNode`] containing the [`Node`] and the
    /// location of the span of text where it was defined.
    ///
    /// # Panics
    ///
    /// Panics if the node does not exist in the tree (it could have been removed).
    pub fn spanned_data(&mut self) -> &mut ParsedNode<'a> {
        &mut self.inner_mut().data
    }

    /// Get a reference to the parent of this node.
    pub fn parent(&mut self) -> TreeRefMut<'a, '_> {
        TreeRefMut {
            id: self.inner().parent,
            tree: self.tree,
        }
    }

    /// Get a reference to the previous sibling of this node.
    pub fn prev_sibling(&'b mut self) -> Option<Self> {
        let children = self.as_ref().parent().children();
        let index = children.iter().position(|tree_ref| tree_ref.id == self.id);

        match index {
            Some(i) if i > 0 => Some(Self {
                id: children[i - 1].id,
                tree: self.tree,
            }),
            _ => None,
        }
    }

    /// Get a reference to the next sibling of this node.
    pub fn next_sibling(&'b mut self) -> Option<Self> {
        let children = self.as_ref().parent().children();
        let index = children.iter().position(|tree_ref| tree_ref.id == self.id);

        match index {
            Some(i) if i + 1 < children.len() => Some(Self {
                id: children[i + 1].id,
                tree: self.tree,
            }),
            _ => None,
        }
    }

    /// Set the text of the [`TreeRef`] while escaping the content using the provided [`Escaping`]
    /// variant (if you are lazy, you can use [`Escaping::default`] as a safe default).
    ///
    /// Replaces **all the children** with the provided text if it is a [`Node::Element`].
    ///
    /// Updates the text content if it is a [`Node::TemplateBlock(TemplateBlock::RawText(_))`](`Node::TemplateBlock`).
    ///
    /// Does nothing if the node is neither a [`Node::Element`] nor a [`Node::TemplateBlock(TemplateBlock::RawText(_))`](`Node::TemplateBlock`).
    ///
    /// [`Node::Element`]: crate::Node::Element
    /// [`Node::Text`]: crate::Node::Text
    pub fn set_text<T: Into<Cow<'a, str>>>(&mut self, new_text: T, escaping: Escaping) {
        match self.as_ref().data() {
            Node::Element(..) => {
                // Remove all the children of this node
                let children = self.as_ref().children_id();

                children
                    .into_iter()
                    .for_each(|child| self.tree.get_mut(child).remove());

                // Escape the text
                let new_text_escaped = escaping.escape(new_text.into());

                // Append a new text node and add it to the list of children
                self.tree.insert(
                    self.id,
                    Spanned::new(Node::TemplateBlock(TemplateBlock::RawText(
                        new_text_escaped,
                    ))),
                );
            }
            Node::TemplateBlock(_) => {
                if let Node::TemplateBlock(TemplateBlock::RawText(ref mut t)) = self.data() {
                    *t = new_text.into();
                }
            }
            _ => (),
        }
    }

    /// Set the value of an attribute belonging to a [`Node::Element`].
    ///
    /// The value of the attribute will be updated if it already exists or a new entry will be
    /// inserted.
    ///
    /// The second argument should be `None` if the attribute has no value.
    pub fn set_attr<A: Into<Cow<'a, str>>, B: Into<Cow<'a, str>>>(
        &mut self,
        key: A,
        val: Option<B>,
        escaping: Escaping,
    ) {
        if let Node::Element(_, ref mut attrs) = self.tree.get_mut(self.id).data() {
            attrs.insert(
                key.into(),
                vec![Spanned::new(TemplateBlock::RawText(
                    val.map_or(Cow::Borrowed(""), |v| escaping.escape(v.into())),
                ))],
            );
        }
    }

    /// Remove an attribute belonging to a [`Node::Element`].
    pub fn remove_attr<A: Into<Cow<'a, str>>>(&mut self, key: A) {
        if let Node::Element(_, ref mut attrs) = self.tree.get_mut(self.id).data() {
            attrs.remove(key.into());
        }
    }

    /// Replace the current [`Node`] with one or more [`Node`]s of a [`Tree`].
    ///
    /// This function consumes the [`TreeRefMut`] to avoid using a node that was removed.
    ///
    /// # Panics
    ///
    /// This function panics if the node is not found in its parent children. If it happens, it is
    /// a bug in `pochoir-parser` not a bug in your code.
    pub fn replace_node(self, tree: &Tree<'a>) {
        // Get the index of the current node in its parent children
        let current_index = self
            .as_ref()
            .parent()
            .inner()
            .children
            .iter()
            .position(|id| *id == self.id)
            .expect("failed to find the node in its parent children");

        let offset = self.tree.next_id();

        // Remove the node and the reference of it in the parent list of children
        let parent_id = self.as_ref().parent().id();

        let mut inserted_nodes = 0;
        let mut max_offset_node = offset;

        for tree_ref in tree.traverse_depth() {
            let is_root_node = tree_ref.parent().id() == TreeRefId::Root;
            let children = tree_ref.children_id().iter().map(|c| *c + offset).collect();

            self.tree.insert_with_id(
                tree_ref.id + offset,
                if is_root_node {
                    parent_id
                } else {
                    tree_ref.parent().id() + offset
                },
                children,
                tree_ref.spanned_data().clone(),
            );

            if tree_ref.id + offset > max_offset_node {
                max_offset_node = tree_ref.id + offset;
            }

            if is_root_node {
                self.tree
                    .get_mut(parent_id)
                    .inner_mut()
                    .children
                    .insert(current_index + inserted_nodes, tree_ref.id() + offset);

                inserted_nodes += 1;
            }
        }

        self.tree.next_id = max_offset_node + 1;

        self.remove();
    }

    /// Prepend the [`Node`]s of a [`Tree`] to the list of children of the current node.
    pub fn prepend_children(&mut self, tree: &Tree<'a>) {
        let offset = self.tree.next_id();
        let mut inserted_nodes = 0;
        let mut max_offset_node = offset;

        for tree_ref in tree.traverse_depth() {
            let is_root_node = tree_ref.parent().id() == TreeRefId::Root;
            let children = tree_ref.children_id().iter().map(|c| *c + offset).collect();

            self.tree.insert_with_id(
                tree_ref.id + offset,
                if is_root_node {
                    self.id()
                } else {
                    tree_ref.parent().id() + offset
                },
                children,
                tree_ref.spanned_data().clone(),
            );

            if tree_ref.id + offset > max_offset_node {
                max_offset_node = tree_ref.id + offset;
            }

            if is_root_node {
                self.inner_mut()
                    .children
                    .insert(inserted_nodes, tree_ref.id() + offset);

                inserted_nodes += 1;
            }
        }

        self.tree.next_id = max_offset_node + 1;
    }

    /// Append the [`Node`]s of a [`Tree`] to the list of children of the current node.
    pub fn append_children(&mut self, tree: &Tree<'a>) {
        let offset = self.tree.next_id();
        let mut max_offset_node = offset;

        for tree_ref in tree.traverse_depth() {
            let is_root_node = tree_ref.parent().id() == TreeRefId::Root;
            let children = tree_ref.children_id().iter().map(|c| *c + offset).collect();

            self.tree.insert_with_id(
                tree_ref.id + offset,
                if is_root_node {
                    self.id()
                } else {
                    tree_ref.parent().id() + offset
                },
                children,
                tree_ref.spanned_data().clone(),
            );

            if tree_ref.id + offset > max_offset_node {
                max_offset_node = tree_ref.id + offset;
            }

            if is_root_node {
                self.inner_mut().children.push(tree_ref.id() + offset);
            }
        }

        self.tree.next_id = max_offset_node + 1;
    }

    /// Replace the current [`Node`] with one or more [`Node`]s of a [`OwnedTree`].
    ///
    /// This function consumes the [`TreeRefMut`] to avoid using a node that was removed.
    ///
    /// # Panics
    ///
    /// This function panics if the node is not found in its parent children. If it happens, it is
    /// a bug in `pochoir-parser` not a bug in your code.
    pub fn replace_node_owned(self, tree: &OwnedTree) {
        // Get the index of the current node in its parent children
        let current_index = self
            .as_ref()
            .parent()
            .inner()
            .children
            .iter()
            .position(|id| *id == self.id)
            .expect("failed to find the node in its parent children");

        let offset = self.tree.next_id();

        // Remove the node and the reference of it in the parent list of children
        let parent_id = self.as_ref().parent().id();

        let mut inserted_nodes = 0;
        let mut max_offset_node = offset;

        for tree_ref in tree.get_tree().traverse_depth() {
            let is_root_node = tree_ref.parent().id() == TreeRefId::Root;
            let children = tree_ref.children_id().iter().map(|c| *c + offset).collect();

            self.tree.insert_with_id(
                tree_ref.id + offset,
                if is_root_node {
                    parent_id
                } else {
                    tree_ref.parent().id() + offset
                },
                children,
                tree_ref
                    .spanned_data()
                    .clone()
                    .map_spanned(Node::deep_clone),
            );

            if tree_ref.id + offset > max_offset_node {
                max_offset_node = tree_ref.id + offset;
            }

            if is_root_node {
                self.tree
                    .get_mut(parent_id)
                    .inner_mut()
                    .children
                    .insert(current_index + inserted_nodes, tree_ref.id() + offset);

                inserted_nodes += 1;
            }
        }

        self.tree.next_id = max_offset_node + 1;

        self.remove();
    }

    /// Prepend the [`Node`]s of a [`OwnedTree`] to the list of children of the current node.
    pub fn prepend_children_owned(&mut self, tree: &OwnedTree) {
        let offset = self.tree.next_id();
        let mut inserted_nodes = 0;
        let mut max_offset_node = offset;

        for tree_ref in tree.get_tree().traverse_depth() {
            let is_root_node = tree_ref.parent().id() == TreeRefId::Root;
            let children = tree_ref.children_id().iter().map(|c| *c + offset).collect();

            self.tree.insert_with_id(
                tree_ref.id + offset,
                if is_root_node {
                    self.id()
                } else {
                    tree_ref.parent().id() + offset
                },
                children,
                tree_ref
                    .spanned_data()
                    .clone()
                    .map_spanned(Node::deep_clone),
            );

            if tree_ref.id + offset > max_offset_node {
                max_offset_node = tree_ref.id + offset;
            }

            if is_root_node {
                self.inner_mut()
                    .children
                    .insert(inserted_nodes, tree_ref.id() + offset);

                inserted_nodes += 1;
            }
        }

        self.tree.next_id = max_offset_node + 1;
    }

    /// Append the [`Node`]s of a [`OwnedTree`] to the list of children of the current node.
    pub fn append_children_owned(&mut self, tree: &OwnedTree) {
        let offset = self.tree.next_id();
        let mut max_offset_node = offset;

        for tree_ref in tree.get_tree().traverse_depth() {
            let is_root_node = tree_ref.parent().id() == TreeRefId::Root;
            let children = tree_ref.children_id().iter().map(|c| *c + offset).collect();

            self.tree.insert_with_id(
                tree_ref.id + offset,
                if is_root_node {
                    self.id()
                } else {
                    tree_ref.parent().id() + offset
                },
                children,
                tree_ref
                    .spanned_data()
                    .clone()
                    .map_spanned(Node::deep_clone),
            );

            if tree_ref.id + offset > max_offset_node {
                max_offset_node = tree_ref.id + offset;
            }

            if is_root_node {
                self.inner_mut().children.push(tree_ref.id() + offset);
            }
        }

        self.tree.next_id = max_offset_node + 1;
    }

    /// Replace the outer HTML of the [`TreeRefMut`].
    ///
    /// # Errors
    ///
    /// Returns an error if parsing the HTML failed.
    pub fn replace_html<T: Into<Cow<'a, str>>>(self, file_path: &str, html: T) -> Result<()> {
        let html = html.into();

        if let Cow::Borrowed(html) = html {
            let tree = crate::parse(file_path, html)?;
            self.replace_node(&tree);
        } else if let Cow::Owned(html) = html {
            let tree = crate::parse_owned(file_path, &html)?;
            self.replace_node_owned(&tree);
        }
        Ok(())
    }

    /// Prepend HTML as children of the current [`TreeRefMut`].
    ///
    /// # Errors
    ///
    /// Returns an error if parsing the HTML failed.
    pub fn prepend_html<T: Into<Cow<'a, str>>>(&mut self, file_path: &str, html: T) -> Result<()> {
        let html = html.into();

        if let Cow::Borrowed(html) = html {
            let tree = crate::parse(file_path, html)?;
            self.prepend_children(&tree);
        } else if let Cow::Owned(html) = html {
            let tree = crate::parse_owned(file_path, &html)?;
            self.prepend_children_owned(&tree);
        }
        Ok(())
    }

    /// Append HTML as children of the current [`TreeRefMut`].
    ///
    /// # Errors
    ///
    /// Returns an error if parsing the HTML failed.
    pub fn append_html<T: Into<Cow<'a, str>>>(&mut self, file_path: &str, html: T) -> Result<()> {
        let html = html.into();

        if let Cow::Borrowed(html) = html {
            let tree = crate::parse(file_path, html)?;
            self.append_children(&tree);
        } else if let Cow::Owned(html) = html {
            let tree = crate::parse_owned(file_path, &html)?;
            self.append_children_owned(&tree);
        }

        Ok(())
    }

    /// Remove the node from the tree.
    ///
    /// This function consumes the [`TreeRefMut`] to avoid using a node that was removed.
    ///
    /// # Panics
    ///
    /// This function panics if the node is not found in its parent children. If it happens, it is
    /// a bug in `pochoir-parser` not a bug in your code.
    pub fn remove(self) {
        // Remove in parent
        let node = &mut self.tree.get_mut(self.id);
        let parent = &mut node.parent();
        let parent_children = &mut parent.inner_mut().children;
        let index = parent_children
            .iter()
            .position(|c| c == &self.id)
            .expect("failed to find node in parent children");
        parent_children.remove(index);

        // Remove children
        let node = self.tree.get(self.id);
        let children = node.inner().children.clone();

        for child in children {
            self.tree.get_mut(child).remove();
        }

        // Remove from tree
        self.tree.nodes.remove(&self.id);
    }
}

impl<'a, 'b> PartialEq for TreeRefMut<'a, 'b> {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl<'a, 'b> fmt::Debug for TreeRefMut<'a, 'b> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("TreeRefMut")
            .field("id", &self.id)
            .field("__inner_node", &self.as_ref().data())
            .finish()
    }
}
