use pochoir_common::Spanned;
use std::result;
use thiserror::Error;

pub type Result<T> = std::result::Result<T, Spanned<Error>>;

#[derive(Error, Debug, PartialEq, Eq, Clone)]
pub enum Error {
    #[error("expected {expected:?}, found {found:?}")]
    UnexpectedInput { expected: String, found: String },

    #[error("invalid tag name: {0:?}")]
    InvalidTagName(String),

    #[error("invalid attribute name: {0:?}")]
    InvalidAttributeName(String),

    #[error("end tag {end_tag:?} found when trying to close {start_tag:?}")]
    UnexpectedEndTagName { start_tag: String, end_tag: String },

    #[error("unexpected end tag {0:?}")]
    UnexpectedEndTag(String),

    #[error("expected tag name")]
    ExpectedTagName,

    #[error("element {0:?} is a void element and must not be closed")]
    ClosedVoidElement(String),

    #[error("missing whitespace between attributes")]
    MissingWhitespaceBetweenAttributes,

    #[error("an end \">\" is missing to close the tag")]
    MissingEndAngleBracket,

    /// An error happening in an event handler.
    #[error("an error happened when executing an event handler: {0:?}")]
    EventHandlerError(String),

    #[error(transparent)]
    TemplateError(#[from] pochoir_template_engine::Error),

    #[error(transparent)]
    StreamParserError(#[from] pochoir_common::Error),
}

pub(crate) trait AutoError<T>
where
    Self: Sized,
{
    fn auto_error(self) -> T;
}

impl<T> AutoError<result::Result<T, Spanned<Error>>>
    for result::Result<T, Spanned<pochoir_common::Error>>
{
    fn auto_error(self) -> result::Result<T, Spanned<Error>> {
        self.map_err(|e| e.map_spanned(Error::StreamParserError))
    }
}

impl<T> AutoError<result::Result<T, Spanned<Error>>>
    for result::Result<T, Spanned<pochoir_template_engine::Error>>
{
    fn auto_error(self) -> result::Result<T, Spanned<Error>> {
        self.map_err(|e| e.map_spanned(Error::TemplateError))
    }
}
