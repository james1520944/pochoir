use criterion::{criterion_group, criterion_main, Criterion};
use pochoir::{compile, component_not_found, lang::Context, ComponentFile};

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("compile", |b| {
        b.iter(|| {
            let html = r#"<ul class="/test/">{{ my_value }}</ul>"#;

            let mut context = Context::new();
            context.insert("my_value", 42);

            compile("template.html", &mut context, |name| {
                if name == "template.html" {
                    Ok(ComponentFile::new_inline(html))
                } else {
                    Err(component_not_found(name))
                }
            })
            .expect("failed to compile template");
        })
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
