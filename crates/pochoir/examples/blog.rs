//! Example of a file-based blog.
//!
//! Each post is presented in a table in the index page with its date, title and description.
//!
//! To add a post, just create a new file in the `posts` directory.
use std::borrow::Cow;

use pochoir::{
    lang::{object, Context},
    providers::FilesystemProvider,
};

pub fn main() {
    let provider = FilesystemProvider::new().with_path("crates/pochoir/examples/blog_templates");

    let posts = std::fs::read_dir("crates/pochoir/examples/blog_templates/posts")
        .expect("directory of posts should be readable")
        .filter_map(|entry| entry.ok())
        .filter_map(|entry| {
            // Here we get the file stem (the file name without extension) and converts it into a
            // String while ignoring all errors
            entry
                .path()
                .file_stem()
                .and_then(|file_stem| file_stem.to_os_string().into_string().ok())
        })
        .map(|file_stem| {
            // We get the file containing the post from the provider which have already scanned
            // and read all files in the `posts` directory, then we parse the HTML using the
            // official parser from `pochoir` and we use CSS selectors to query certain elements
            // of the HTML tree to fill the table with the right information
            let component_file = provider
                .get(&file_stem)
                .expect("post should be present in provider");

            let post_tree = pochoir::parser::parse(component_file.path, &component_file.data)?;

            let title = post_tree
                .select("h1")
                .map_or(Cow::Borrowed("Unnamed post"), |id| post_tree.get(id).text());

            let mut description = post_tree
                .select("p")
                .map_or(Cow::Borrowed("No description"), |id| {
                    post_tree.get(id).text()
                });

            if description.len() > 100 {
                // Description should have at most 100 characters
                description = Cow::Owned(description[..99].to_string() + "…");
            }

            Ok(object! {
                "title" => title,
                "description" => description,

                // The post files are named with the date at which they were created
                "date" => file_stem,
            })
        })
        .collect::<pochoir::parser::Result<Vec<_>>>()
        .expect("parsing should not fail");

    let mut context = Context::from_object(object! {
        "posts" => posts,
    });

    println!(
        "{}",
        provider
            .compile("index", &mut context)
            .expect("compilation should not fail")
    );
}
