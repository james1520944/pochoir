//! Template providers are high-level structures storing the source of templates and components and
//! providing it to the compiler.
//!
//! You can generally insert templates in them using either the
//! builder pattern (`with_*` methods) or using an imperative API (`insert_*` methods). Two
//! official providers are available: the [`FilesystemProvider`] gets the source HTML from
//! the files in a directory, and the [`StaticMapProvider`] stores source files in a map
//! with the component name as key.
//!
//! ##### Example
//!
//! ```no_run
//! # fn main() -> pochoir::Result<()> {
//! use pochoir::{Context, FilesystemProvider};
//!
//! // The `FilesystemProvider` selects files using two criterias: if they have a
//! // known extension (they can be configured, by default just `html` files can be
//! // used) and if they are in one of the inserted path. Here all files in the
//! // `templates` directory having a `.html` extension will be used
//! let provider = FilesystemProvider::new().with_path("templates");
//! let mut context = Context::new();
//!
//! let _html = provider.compile("index", &mut context)?;
//! # Ok(())
//! # }
//! ```
//!
//! ```
//! # fn main() -> pochoir::Result<()> {
//! use pochoir::{Context, StaticMapProvider};
//!
//! // The last argument is the path to the file if it was read from
//! // the filesystem, it is used in error messages to find the HTML file source
//! let provider = StaticMapProvider::new().with_template("index", "<h1>Index page</h1>", None);
//! let mut context = Context::new();
//!
//! let _html = provider.compile("index", &mut context)?;
//! # Ok(())
//! # }
//! ```
//! If you want more control over how the source files are fetched, you can use the closure API and
//! the [`compile`](`crate::compile`) function.
mod fs;
mod static_map;

pub use fs::FilesystemProvider;
pub use static_map::StaticMapProvider;
