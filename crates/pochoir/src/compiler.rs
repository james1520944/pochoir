//! The Server-side Web Components compiler
//!
//! ## Features
//!
//! ### Variables and expressions
//!
//! Variables needs to be defined in a *[`Context`]*. They can be inserted using [`Context::insert`]
//! but if you want child components to also inherit the data (like a global variable), you should
//! use [`Context::insert_inherited`]. Both functions take a key as first argument which is the name
//! of the variable usable in expressions and a value implementing the [`IntoValue`] trait
//! (implemented for a *lot* of default types and structures). The value will then be cloned and
//! transformed in each expression. If you want to pass enumerations or structures, you should
//! implement [`IntoValue`] on them using the [`IntoValue`] derive macro. The variable keys must
//! follow some rules to be valid: they must only contain lowercase and uppercase Latin letters,
//! underscores and digits (but the first character must not be a digit).
//!
//! When data needs to be inserted inside a page, you need to use an *expression*. Expressions are
//! tiny groups of variables and operators written in their own [custom language](`pochoir_lang`) that
//! manipulate data. They are written in a pair of curly brackets and can be used everywhere you write text.
//! The resulting value of expressions is escaped to prevent XSS attacks, but it is possible to opt
//! out of auto-escaping by replacing the inner curly brackets by exclamation marks.
//!
//! [`IntoValue`]: pochoir_lang::IntoValue
//!
//! ##### Example
//!
//! ```
//! // In a Rust file
//! use pochoir::Context;
//!
//! let mut context = Context::new();
//! context.insert("date", "August 26, 2023");
//! context.insert("html", "<b>some bold HTML</b>");
//! ```
//!
//! ```html
//! {# In an HTML file #}
//! Today it is {{ date }}.
//! Unescaped HTML can be inserted: {! html !}.
//! ```
//!
//! Will render as:
//!
//! ```html
//! Today it is August 26, 2023.
//! Unescaped HTML can be inserted: <b>some bold HTML</b>.
//! ```
//!
//! ### Statements
//!
//! Common statements can be used in templates. They are all written in curly brackets with inner
//! percentages.
//!
//! - `if`/`elif`/`else` *conditional statements* are used to check if an
//! expression equals `true`. If it does, the inner content will be included, if not it won't. One
//! additional feature is that you can use the `if let` (and `elif let`) syntax to check
//! if a value is not null. It can be combined with an assignment to replicate the
//! `if let Some(_) = _` syntax of Rust: "unwrap" the value if it is not null or don't
//! execute a block content at all if the value is null. You need to note that assignments
//! return the assigned value (like in Javascript) which enables the `if let` syntax to do that
//!
//! - `for` *loop statements* are also supported. They are mostly used to iterate lists
//! so they are written using the `for ... in ...` syntax. If you want to just get some ordered numbers,
//! you would need to use ranges like `for ... in 3..12`. *Destructuring objects and arrays* is also
//! supported, so if you iterate an array of objects and they all share the same structure you can
//! bind their the fields to comma-separated variables instead of indexing them later. The same thing is
//! supported for arrays, except that you can name the keys as you want, just the order in which
//! they are defined is important. If a value cannot be destructured (because the field does not exist),
//! the value will simply be `null`
//!
//! - Finally, `let` statements can be used to assign a value to a variable and
//! comments can be added with curly brackets with inner `#`s
//!
//! ##### Example
//!
//! ```
//! // In a Rust file
//! use pochoir::{object, Context};
//!
//! let mut context = Context::new();
//! context.insert("date", "August 26, 2023");
//! context.insert("weather", "cloudy");
//! context.insert("users", vec![
//!     object! {
//!         "name" => "John",
//!         "job" => "Football player"
//!     },
//!     object! {
//!         "name" => "Jane",
//!         "job" => "Designer",
//!     }
//! ]);
//! ```
//!
//! ```html
//! {# In an HTML file #}
//! {% if date == "August 26, 2023" %}
//! It is today!
//! {% elif date == "August 25, 2023" %}
//! It is yesterday!
//! {% else %}
//! Oh welcome to the future.
//! {% endif %}
//!
//! {# Note that a single `=` is used here because it is an assignment. If `weather` is null, the
//! assignment will return `null` and the `if` block content will never run (because `if let`
//! checks for `null`ity) but if the value is something other than `null`, `current_weather` will
//! be different than `null` and the inner block will be run #}
//!
//! {% if let current_weather = weather %}
//! Today it is {{ current_weather }}.
//! {% endif %}
//!
//! {# We now define a variable in the template and iterate it in a for loop #}
//!
//! {% let alphabet = ["a", "b", "c", "d"] %}
//!
//! {% for letter in alphabet %}"{{ letter }}" then {% endfor %}"z"
//!
//! {# Objects are destructured here: the `name` and `job` fields will be extracted from the
//! objects to avoid having to later index them #}
//!
//! <ul>
//!   {% for name, job in users %}
//!   <li>{{ name }} is a {{ job }}</li>
//!   {% endfor %}
//! </ul>
//! ```
//!
//! Will render as (some newlines are ignored for clarity):
//!
//! ```html
//! It is today!
//!
//! Today it is cloudy.
//!
//! "a" then "b" then "c" then "d" then "z"
//!
//! <ul>
//!   <li>John is a Football player</li>
//!   <li>Jane is a Designer</li>
//! </ul>
//! ```
//!
//! ### What are providers?
//!
//! Providers are high-level structures storing the source of templates and components and
//! providing it to the compiler. You can generally insert templates in them using either the
//! builder pattern (`with_*` methods) or using an imperative API (`insert_*` methods). Two
//! official providers are available: the [`FilesystemProvider`] gets the source HTML from
//! the files in a directory, and the [`StaticMapProvider`] stores source files in a map
//! with the component name as key.
//!
//! ##### Example
//!
//! ```no_run
//! # fn main() -> pochoir::Result<()> {
//! use pochoir::{Context, FilesystemProvider};
//!
//! // The `FilesystemProvider` selects files using two criterias: if they have a
//! // known extension (they can be configured, by default just `html` files can be
//! // used) and if they are in one of the inserted path. Here all files in the
//! // `templates` directory having a `.html` extension will be used
//! let provider = FilesystemProvider::new().with_path("templates");
//! let mut context = Context::new();
//!
//! let _html = provider.compile("index", &mut context)?;
//! # Ok(())
//! # }
//! ```
//!
//! ```
//! # fn main() -> pochoir::Result<()> {
//! use pochoir::{Context, StaticMapProvider};
//!
//! // The last argument is the path to the file if it was read from
//! // the filesystem, it is used in error messages to find the HTML file source
//! let provider = StaticMapProvider::new().with_template("index", "<h1>Index page</h1>", None);
//! let mut context = Context::new();
//!
//! let _html = provider.compile("index", &mut context)?;
//! # Ok(())
//! # }
//! ```
//!
//! But if you want more control over how the source files are fetched, you can use
//! the closure API by directly using the [`compile`] function. The closure
//! takes the name of the component and returns a [`ComponentFile`] with the file name and
//! data.
//!
//! ```
//! # fn main() -> pochoir::Result<()> {
//! use pochoir::{Context, ComponentFile, error};
//! use std::path::Path;
//!
//! let html = pochoir::compile("index", &mut Context::new(), |name| {
//!     // In a real world usage, you would fetch the HTML from a complex, dynamic,
//!     // pipeline of sources, maybe from the network.
//!     // A `ComponentFile` is used to associate some data with a path to a file,
//!     // if it was fetched from the filesystem. You can use
//!     // `ComponentFile::new_inline` if you don't want to provide a path, in this
//!     // case the path will simply be `inline`
//!     Ok(match name {
//!         "index" => ComponentFile::new_inline("<h1>Index page</h1><my-button />"),
//!         "my-button" => ComponentFile::new(Path::new("my-button.html"), "<button>Click me!</button>"),
//!         _ => return Err(error::component_not_found(name)),
//!     })
//! })?;
//! # Ok(())
//! # }
//! ```
//!
//! [`StaticMapProvider`]: crate::StaticMapProvider
//! [`FilesystemProvider`]: crate::FilesystemProvider
//!
//! ### Components
//!
//! > From now on, we'll be using full Rust files as example with the [`StaticMapProvider`] to insert
//! > HTML sources but it is also perfectly possible to use the [`FilesystemProvider`] or the closure
//! > API, it is just easier to show here.
//!
//! Components can be defined using two ways: either you just *insert them in the provider of your
//! choice* (you can also return a [`ComponentFile`] in the component resolver) or you define them
//! using a *`<template>` element* directly in the HTML file. The components defined in the latter
//! case are **scoped to their parent and cannot be used before they are declared** to avoid
//! polluting the complete component. In both cases you cam use them as custom elements by
//! *writing an element having the name* of the template in the HTML. The name of components must
//! follow some rules to be compatible with custom elements: **they must contain a dash, start with
//! a lowercase letter and must not be one of the reserved element names** (see
//! <https://html.spec.whatwg.org/multipage/custom-elements.html#valid-custom-element-name> for
//! more information). Moreover, each component name **should be unique**: if a component is
//! redefined it is overriden, however the old component cannot be inserted in the new one and
//! having two components with the same name can be incompatible with some transformers, e.g
//! `pochoir_extra::EnhancedCss`.
//!
//! ##### Example of components
//!
//! ```
//! # fn main() -> pochoir::Result<()> {
//! use pochoir::{Context, StaticMapProvider};
//!
//! let provider = StaticMapProvider::new()
//!     .with_template("index", "<my-button></my-button>", None)
//!     .with_template("my-button", "<button>Click me!</button>", None);
//! let mut context = Context::new();
//!
//! let html = provider.compile("index", &mut context)?;
//!
//! assert_eq!(html, "<button>Click me!</button>");
//! # Ok(())
//! # }
//! ```
//!
//! Components can have *properties* passed from the parent to the component using HTML attributes.
//! By default, they are interpreted as strings (because HTML attributes *are* strings) except if
//! they contain an expression.
//! The keys of attributes should be in [kebab-case](https://en.wikipedia.org/wiki/Letter_case#Kebab_case)
//! (with dashes) but they will be converted into
//! [snake_case](https://en.wikipedia.org/wiki/Letter_case#Snake_case) (with underscores)
//! to be used in expressions.
//!
//! ##### Example of component properties
//!
//! ```
//! # fn main() -> pochoir::Result<()> {
//! use pochoir::{Context, StaticMapProvider};
//!
//! let provider = StaticMapProvider::new()
//!     .with_template("index", r#"
//!     <my-button some-classes="foo bar" count="{{ 2 }}" primary></my-button>"#, None)
//!     .with_template("my-button", r#"
//!     <button class="{{ some_classes }} {{ primary != null ? 'is-primary' : 'is-secondary' }}">
//!       Count: {{ count + 1 }}
//!     </button>"#, None);
//! let mut context = Context::new();
//!
//! let html = provider.compile("index", &mut context)?;
//!
//! assert_eq!(html, r#"
//!    
//!     <button class="foo bar is-primary">
//!       Count: 3
//!     </button>"#);
//! # Ok(())
//! # }
//! ```
//!
//! Components can have *slots*, HTML children passed from the parent to the component. By default,
//! all children of a component instance will be given as the default slot, but you can have
//! children contained in a *named slot* using the `slot` attribute. The slots can then be inserted
//! into the component HTML by using `<slot>` elements which can have the `name` attribute for
//! inserting named slots. If you need to pass variables from the component to the parent
//! slot, you can just give attributes to the `<slot>` element, they will then be usable in the
//! parent `<slot>` element. This process is called
//! [scoped slots](https://vuejs.org/guide/components/slots.html#scoped-slots). Finally, if you
//! want to make a slot optional and use some default slot content when no one is given, you can
//! just write elements *inside* the `<slot>` elements.
//!
//! ##### Example of component slots
//!
//! ```
//! # fn main() -> pochoir::Result<()> {
//! use pochoir::{Context, StaticMapProvider};
//!
//! let provider = StaticMapProvider::new()
//!     .with_template("index", r#"
//!     <my-card>
//!         <h1 slot="header">My header</h1>
//!         <p>Card content</p>
//!         All nodes not having a `slot` attribute belong to the default slot
//!     </my-card>"#, None)
//!     .with_template("my-card", r#"
//!     <div>
//!       <header>
//!         <slot name="header"></slot>
//!       </header>
//!       <main>
//!         <slot></slot>
//!       </main>
//!       <footer>
//!         <slot name="footer">Default footer content</slot>
//!       </footer>
//!     </div>"#, None);
//! let mut context = Context::new();
//!
//! let html = provider.compile("index", &mut context)?;
//!
//! assert_eq!(html, r#"
//!    
//!     <div>
//!       <header>
//!         <h1>My header</h1>
//!       </header>
//!       <main>
//!        
//!        
//!         <p>Card content</p>
//!         All nodes not having a `slot` attribute belong to the default slot
//!    
//!       </main>
//!       <footer>
//!         Default footer content
//!       </footer>
//!     </div>"#);
//! # Ok(())
//! # }
//! ```
//!
//! ### Quirks
//!
//! Nested objects defined in template expressions need to have spaces between their curly braces
//! to differentiate them from template expression delimiters.
//!
//! **Bad**:
//!
//! ```text
//! {{ {a: {b: "letters"}} }}
//! ```
//!
//! The error will be:
//!
//! <style>
//! pre#no-styles {
//!   background-color: initial;
//!   padding: 0px;
//!   line-height: initial;
//!   padding: 16px;
//!   border: 2px solid #D5D5D5;
//! }
//! </style>
//!
//! <pre id="no-styles" style="font-family: monospace, sans-serif;"><span style="color: red; font-weight: 600;">error</span><span style="font-weight: 600;">: unterminated object</span>
//! <span style="margin-left: 2rem; color: #666; font-size: 0.9em;">./index.html:1:7</span>
//!
//! <code style="background-color: #F5F5F5; color: black; width: max-content; display: inline-block; padding: 14px; line-height: 1.5;">1 | {{ {a:<span style="font-weight: 900; color: red;">{</span>b: "letters"}} }}</code></pre>
//!
//! **Fixed**:
//!
//! ```text
//! {{ {a: {b: "letters"} } }}
//! ```
//!
//! ### Where to go next?
//!
//! - Check out [the `pochoir-lang` crate](`pochoir_lang`) to learn more about what syntax you can
//! use in expressions (like how to do ranges, how to make or call a function)
//! - Check out [the examples](https://gitlab.com/encre-org/pochoir/-/tree/main/crates/pochoir/examples) to better know what is possible to do with `pochoir`
//! - Check out [the documentation about transformers](`crate::transformers`), they are used to manipulate HTML trees
//! - Check out [the documentation about providers](`crate::providers`), they are used to store
//! your component sources
//!
//! ### Errors
//!
//! `pochoir` and its sibling crates use a flexible error management system based on the [`Spanned`] structure that you have to
//! check in order to get the span of text and the file name producing the error. `pochoir` adds a
//! third value that you can get: the name of the component throwing the error by using the
//! [`SpannedWithComponent`] wrapper structure. Moreover the [`Error`] structure of the `pochoir`
//! crate uses the [`AutoError`] trait to convert all errors of sibling crates (`pochoir-template-engine`,
//! `pochoir-lang`, …) to the single [`Error`] structure of the main crate).
//!
//! ### Playground
//!
//! A web playground using `WebAssembly` is available at <https://encre-org.gitlab.io/pochoir-playground>
//! to mess with the syntax.
//!
//! [`Context::insert_inherited`]: Context::insert_inherited
//! [`eval`]: crate::eval
use convert_case::{Case, Casing};
use std::{
    borrow::{Borrow, Cow},
    collections::HashMap,
    fmt::Write,
    path::Path,
};

use crate::common::{Spanned, StreamParser};
use crate::component_file::ComponentFile;
use crate::error::{
    AutoError, AutoErrorWithName, AutoErrorWithNameOffset, Error, Result, SpannedWithComponent,
};
use crate::lang::{Context, Value};
use crate::parser::{Node, ParseEvent, Tree, TreeRefId, EMPTY_HTML_ELEMENTS};
use crate::template_engine::{Escaping, TemplateBlock};
use crate::transformers::Transformers;

/// Tests if an element is a custom element using its name.
///
/// <https://html.spec.whatwg.org/multipage/custom-elements.html#valid-custom-element-name>
pub fn is_custom_element(name: &str) -> bool {
    name.starts_with(char::is_lowercase)
        && name.contains('-')
        && ![
            "annotation-xml",
            "color-profile",
            "font-face",
            "font-face-src",
            "font-face-uri",
            "font-face-format",
            "font-face-name",
            "missing-glyph",
        ]
        .contains(&name)
}

#[derive(Debug)]
struct CompilationContext<'a, 'b, 'c, 'd, 'e, 'f, 'g> {
    name: &'a str,
    context: &'b mut Context,
    tree: &'c Tree<'d>,
    node_ids: Vec<TreeRefId>,
    slots: &'e HashMap<Cow<'f, str>, Vec<TreeRefId>>,
    parent: Option<&'g CompilationContext<'a, 'b, 'c, 'd, 'e, 'f, 'g>>,
}

#[allow(clippy::too_many_lines, clippy::too_many_arguments)]
fn eval_stmt<'a, 'b, 'c, 'd>(
    stmt_expr: &str,
    ctx: &mut CompilationContext<'_, '_, '_, 'a, '_, '_, '_>,
    component_resolver: &mut impl FnMut(&str) -> Result<ComponentFile<'c, 'd>>,
    node_index: &mut usize,
    result: &mut String,
    components: Cow<HashMap<String, Tree<'a>>>,
    file_offset: usize,
    transformers: &mut Transformers,
) -> Result<()> {
    let mut parser = StreamParser::new(ctx.tree.file_path(), stmt_expr);

    if parser.take_exact("if ").is_ok() {
        parser.trim();
        let is_let = parser.take_exact("let ").is_ok();
        parser.trim();
        let expr_index = parser.index();
        let expr = parser.take_until_eoi().trim();

        let mut if_branches = vec![(expr.to_string(), is_let, vec![])];
        let mut else_branch = vec![];
        let mut in_else = false;
        let mut count_if = 0;

        while *node_index < ctx.node_ids.len() {
            let node_id = ctx.node_ids[*node_index];
            let node = ctx.tree.get(node_id);
            *node_index += 1;

            match &node.data() {
                Node::TemplateBlock(TemplateBlock::Stmt(s)) if s.starts_with("if ") => {
                    if in_else {
                        else_branch.push(node_id);
                    } else {
                        if_branches.last_mut().unwrap().2.push(node_id);
                    }
                    count_if += 1;
                }
                Node::TemplateBlock(TemplateBlock::Stmt(s)) if s.starts_with("elif ") => {
                    if count_if > 1 {
                        if in_else {
                            else_branch.push(node_id);
                        } else {
                            if_branches.last_mut().unwrap().2.push(node_id);
                        }
                    } else if in_else {
                        return Err(SpannedWithComponent::new(Error::ElifAfterElse)
                            .with_span(node.spanned_data().span().clone())
                            .with_file_path(ctx.tree.file_path())
                            .with_component_name(ctx.name));
                    } else {
                        let val = s.strip_prefix("elif ").unwrap().trim();
                        let (is_let, val) = if let Some(val) = val.strip_prefix("let ") {
                            (true, val.trim_start())
                        } else {
                            (false, val)
                        };
                        if_branches.push((val.to_string(), is_let, vec![]));
                    }
                }
                Node::TemplateBlock(TemplateBlock::Stmt(s)) if s == "endif" => {
                    if count_if == 0 {
                        break;
                    }

                    if in_else {
                        else_branch.push(node_id);
                    } else {
                        if_branches.last_mut().unwrap().2.push(node_id);
                    }
                    count_if -= 1;
                }
                Node::TemplateBlock(TemplateBlock::Stmt(s)) if s == "else" => {
                    if count_if == 0 {
                        in_else = true;
                    } else if in_else {
                        else_branch.push(node_id);
                    } else {
                        if_branches.last_mut().unwrap().2.push(node_id);
                    }
                }
                _ if in_else => else_branch.push(node_id),
                _ => if_branches.last_mut().unwrap().2.push(node_id),
            }
        }

        // Evaluate the conditions
        let mut if_matched = None;

        for (i, branch) in if_branches.iter().enumerate() {
            let cond_result = pochoir_lang::eval(
                ctx.tree.file_path(),
                &branch.0,
                ctx.context,
                file_offset + expr_index,
            )
            .auto_error()?;
            if (branch.1 && cond_result != Value::Null) || cond_result == Value::Bool(true) {
                if_matched = Some(i);
                break;
            }
        }

        if let Some(if_matched) = if_matched {
            let old_node_ids = ctx.node_ids.clone();
            ctx.node_ids = if_branches.remove(if_matched).2;
            compile_recursive(ctx, component_resolver, result, components, transformers)?;
            ctx.node_ids = old_node_ids;
        } else {
            let old_node_ids = ctx.node_ids.clone();
            ctx.node_ids = else_branch;
            compile_recursive(ctx, component_resolver, result, components, transformers)?;
            ctx.node_ids = old_node_ids;
        }
    } else if parser.take_exact("for ").is_ok() {
        parser.trim();
        let mut aliases = vec![];

        loop {
            parser.trim();

            let mut first = true;
            let alias = parser
                .take_while(|(_, ch)| {
                    if first {
                        first = false;
                        ch.is_alphabetic() || ch == '_'
                    } else {
                        ch.is_alphanumeric() || ch == '_'
                    }
                })
                .trim();
            aliases.push(alias);

            if parser.take_exact(",").is_err() {
                break;
            }
        }

        parser.trim();
        parser
            .take_exact("in ")
            .auto_error_with_name_offset(ctx.name, file_offset)?;
        parser.trim();

        let expr_index = parser.index();
        let expr = parser.take_until_eoi().trim();
        let expr_end_index = parser.index();

        let mut for_branch = vec![];
        let mut count_for = 0;

        while *node_index < ctx.node_ids.len() {
            let node_id = ctx.node_ids[*node_index];
            let node = ctx.tree.get(node_id);
            *node_index += 1;

            match &node.data() {
                Node::TemplateBlock(TemplateBlock::Stmt(s)) if s.starts_with("for ") => {
                    for_branch.push(node_id);
                    count_for += 1;
                }
                Node::TemplateBlock(TemplateBlock::Stmt(s)) if s == "endfor" => {
                    if count_for == 0 {
                        break;
                    }

                    for_branch.push(node_id);
                    count_for -= 1;
                }
                _ => for_branch.push(node_id),
            }
        }

        // Evaluate the list of values
        let old_values = aliases
            .iter()
            .map(|a| ctx.context.get(*a).cloned())
            .collect::<Vec<Option<Value>>>();
        let old_node_ids = ctx.node_ids.clone();

        let interpreted_val = crate::lang::eval(
            ctx.tree.file_path(),
            expr,
            ctx.context,
            file_offset + expr_index,
        )
        .auto_error_with_name(ctx.name)?;

        if let Value::Array(array) = interpreted_val {
            for item in array {
                if aliases.len() == 1 {
                    ctx.context.insert(aliases[0], item);
                } else {
                    match item {
                        Value::Array(array) => {
                            // The value can be destructured using the order of aliases
                            for (i, alias) in aliases.iter().enumerate() {
                                ctx.context
                                    .insert(*alias, array.get(i).cloned().unwrap_or(Value::Null));
                            }
                        }
                        Value::Object(object) => {
                            // The value can be destructured using the keys
                            for alias in &aliases {
                                ctx.context.insert(
                                    *alias,
                                    object.get(*alias).cloned().unwrap_or(Value::Null),
                                );
                            }
                        }
                        _ => {
                            // The value cannot be destructured, make all aliases null
                            for alias in &aliases {
                                ctx.context.insert(*alias, Value::Null);
                            }
                        }
                    }
                }

                ctx.node_ids = for_branch.clone();

                compile_recursive(
                    ctx,
                    component_resolver,
                    result,
                    Cow::Borrowed(&*components),
                    transformers,
                )?;
            }
        } else if let Value::Range(start, end) = interpreted_val {
            let range = match (start, end) {
                (Some(start), Some(end)) => start..end,
                _ => {
                    return Err(SpannedWithComponent::new(Error::UnboundedRangeInForLoop)
                        .with_span(file_offset + expr_index..file_offset + expr_end_index)
                        .with_file_path(ctx.tree.file_path())
                        .with_component_name(ctx.name));
                }
            };

            for item in range {
                if aliases.len() == 1 {
                    ctx.context.insert(aliases[0], item);
                } else {
                    for alias in &aliases {
                        ctx.context.insert(*alias, Value::Null);
                    }
                }

                ctx.node_ids = for_branch.clone();

                compile_recursive(
                    ctx,
                    component_resolver,
                    result,
                    Cow::Borrowed(&*components),
                    transformers,
                )?;
            }
        }

        ctx.node_ids = old_node_ids;

        for (i, old_val) in old_values.into_iter().enumerate() {
            if let Some(old_val) = old_val {
                ctx.context.insert(aliases[i], old_val);
            } else {
                ctx.context.remove(aliases[i]);
            }
        }
    } else if parser.take_exact("let ").is_ok() {
        parser.trim();
        let mut first = true;
        let name = parser
            .take_while(|(_, ch)| {
                if first {
                    first = false;
                    ch.is_alphabetic() || ch == '_'
                } else {
                    ch.is_alphanumeric() || ch == '_'
                }
            })
            .trim();
        parser.trim();
        parser
            .take_exact("=")
            .auto_error_with_name_offset(ctx.name, file_offset)?;
        parser.trim();
        let expr_index = parser.index();
        let expr = parser.take_until_eoi().trim();

        let val_evaluated = crate::lang::eval(
            ctx.tree.file_path(),
            expr,
            ctx.context,
            file_offset + expr_index,
        )
        .auto_error_with_name(ctx.name)?;

        ctx.context.insert((*name).to_string(), val_evaluated);
    } else {
        return Err(SpannedWithComponent::new(Error::UnknownStatement {
            stmt: stmt_expr[..stmt_expr
                .find(char::is_whitespace)
                .unwrap_or(stmt_expr.len())]
                .to_string(),
        })
        .with_span(
            // The last node was the statement node, that's why we need to use node_index - 1
            ctx.tree
                .get(ctx.node_ids[*node_index - 1])
                .spanned_data()
                .span()
                .clone(),
        )
        .with_file_path(ctx.tree.file_path())
        .with_component_name(ctx.name));
    }

    Ok(())
}

fn render_template_block<'a, 'b, 'c, 'd, 'e, T: Borrow<TemplateBlock<'e>>>(
    ctx: &mut CompilationContext<'_, '_, '_, 'a, '_, '_, '_>,
    component_resolver: &mut impl FnMut(&str) -> Result<ComponentFile<'c, 'd>>,
    node_index: &mut usize,
    blocks: &[Spanned<T>],
    result: &mut String,
    components: &HashMap<String, Tree<'a>>,
    transformers: &mut Transformers,
) -> Result<()> {
    for block in blocks {
        match (**block).borrow() {
            TemplateBlock::RawText(text) => {
                result.push_str(text);
            }
            TemplateBlock::Expr(expr, escape) => {
                let expr_evaluated =
                    crate::lang::eval(ctx.tree.file_path(), expr, ctx.context, block.span().start)
                        .auto_error_with_name(ctx.name)?
                        .to_string();

                if *escape {
                    result.push_str(&Escaping::Html.escape(&expr_evaluated));
                } else {
                    result.push_str(&expr_evaluated);
                }
            }
            TemplateBlock::Stmt(stmt_expr) => {
                eval_stmt(
                    stmt_expr,
                    ctx,
                    component_resolver,
                    node_index,
                    result,
                    Cow::Borrowed(components),
                    block.span().start,
                    transformers,
                )?;
            }
        }
    }

    Ok(())
}

fn compile_template_block<'a, 'b, 'c, 'd>(
    ctx: &mut CompilationContext<'_, '_, '_, 'a, '_, '_, '_>,
    component_resolver: &mut impl FnMut(&str) -> Result<ComponentFile<'c, 'd>>,
    node_index: &mut usize,
    blocks: &[Spanned<TemplateBlock>],
    in_attr: bool,
    components: &HashMap<String, Tree<'a>>,
    transformers: &mut Transformers,
) -> Result<Value> {
    let mut evaluated_value = Value::String(String::new());

    for (i, block) in blocks.iter().enumerate() {
        let val = match &**block {
            TemplateBlock::RawText(text) => Value::String(text.to_string()),
            TemplateBlock::Expr(expr, _) => {
                crate::lang::eval(ctx.tree.file_path(), expr, ctx.context, block.span().start)
                    .auto_error_with_name(ctx.name)?
            }
            TemplateBlock::Stmt(stmt_expr) => {
                if in_attr {
                    // If an attribute contains a statement, its value will always be a string so
                    // we can consider all nodes as string and use the render function of the
                    // template engine on **all** blocks
                    let val: Cow<Value> = Cow::Owned(Value::String(
                        crate::template_engine::render_template(&blocks[i..], ctx.context)
                            .auto_error_with_name(ctx.name)?,
                    ));

                    return Ok(Value::String(
                        evaluated_value.to_string() + &val.to_string(),
                    ));
                }

                let mut value_string = String::new();
                eval_stmt(
                    stmt_expr,
                    ctx,
                    component_resolver,
                    node_index,
                    &mut value_string,
                    Cow::Borrowed(components),
                    block.span().start,
                    transformers,
                )?;

                Value::String(value_string)
            }
        };

        if val == Value::Null {
            // Ignore Null values
        } else if !matches!(&evaluated_value, Value::String(ref s) if !s.is_empty()) {
            evaluated_value = val;
        } else {
            evaluated_value = Value::String(evaluated_value.to_string() + &val.to_string());
        }
    }

    Ok(evaluated_value)
}

fn parse_and_tranform<'a>(
    file_path: &Path,
    component_name: &str,
    data: &'a str,
    transformers: &mut Transformers,
) -> Result<Tree<'a>> {
    let mut builder = pochoir_parser::Builder::new();

    if !transformers.inner.is_empty() {
        builder = builder.on_event(|event, tree, id| match event {
            ParseEvent::BeforeElement => transformers
                .inner
                .iter_mut()
                .try_for_each(|t| t.on_before_element(tree, id)),
            ParseEvent::AfterElement => transformers
                .inner
                .iter_mut()
                .try_for_each(|t| t.on_after_element(tree, id)),
        });
    }

    let mut tree = builder
        .parse(file_path, data)
        .auto_error_with_name(component_name)?;

    if !transformers.inner.is_empty() {
        transformers
            .inner
            .iter_mut()
            .try_for_each(|t| t.on_tree_parsed(&mut tree, component_name, file_path))
            .map_err(|e| {
                SpannedWithComponent::new(Error::ParserError(
                    crate::parser::Error::EventHandlerError(e.to_string()),
                ))
                .with_file_path(file_path)
                .with_component_name(component_name)
            })?;
    }

    Ok(tree)
}

/// Compile template file with full control over how the source files are fetched.
///
/// This function uses a closure (the *component resolver*) taking the name of the component and
/// returning a [`ComponentFile`] with the file name and data.
///
/// ```
/// # fn main() -> pochoir::Result<()> {
/// use pochoir::{Context, ComponentFile, error};
/// use std::path::Path;
///
/// let html = pochoir::compile("index", &mut Context::new(), |name| {
///     // In a real world usage, you would fetch the HTML from a complex, dynamic,
///     // pipeline of sources, maybe from the network.
///     // A `ComponentFile` is used to associate some data with a path to a file,
///     // if it was fetched from the filesystem. You can use
///     // `ComponentFile::new_inline` if you don't want to provide a path, in this
///     // case the path will simply be `inline`
///     Ok(match name {
///         "index" => ComponentFile::new_inline("<h1>Index page</h1><my-button />"),
///         "my-button" => ComponentFile::new(Path::new("my-button.html"), "<button>Click me!</button>"),
///         _ => return Err(error::component_not_found(name)),
///     })
/// })?;
/// # Ok(())
/// # }
/// ```
///
/// If you want high level structures taking crate of source for you, you can use [`providers`](`crate::providers`).
///
/// # Errors
///
/// It is up to you to format runtime errors (e.g using [`error::display_ansi_error`]
/// or [`error::display_html_error`]).
///
/// For example, to display the error using ANSI escape codes (to be used in shells) you can
/// call it like this:
///
/// ```
/// use pochoir::{compile, Context, ComponentFile};
///
/// let source = "<h1>A title</h1>{{ [1, 2]['abc'] }}";
/// let compiled = pochoir::compile("index", &mut Context::new(), |name| {
///     Ok(ComponentFile::new_inline(source))
/// }).map_err(|e| {
///     // Runtime error formatting happens here, it uses
///     // `pochoir::common::Spanned::component_name` to get
///     // the name of the component which raised the error and
///     // fetches it from the provider to get the text source of
///     // the component
///     pochoir::error::display_ansi_error(
///         &e,
///         &source,
///     )
/// });
///
/// assert_eq!(
///     compiled,
///     Err("\u{1b}[1;31merror\u{1b}[0m\u{1b}[1m:\u{1b}[0m \u{1b}[1marray cannot be indexed by String, the index must be a positive number or a range\u{1b}[0m\n   inline:1:27\n\n \u{1b}[1;34m1\u{1b}[0m \u{1b}[1;34m|\u{1b}[0m <h1>A title</h1>{{ [1, 2][\u{1b}[1;31m'abc'\u{1b}[0m] }}".to_string())
/// );
/// ```
///
/// [`error::display_ansi_error`]: crate::error::display_ansi_error
/// [`error::display_html_error`]: crate::error::display_html_error
pub fn compile<'a, 'b, 'c>(
    default_component_name: &str,
    default_context: &mut Context,
    component_resolver: impl FnMut(&str) -> Result<ComponentFile<'b, 'c>>,
) -> Result<String> {
    transform_and_compile(
        default_component_name,
        default_context,
        component_resolver,
        &mut Transformers::new(),
    )
}

/// Compile each template while applying transformers.
///
/// See [`transformers`](`crate::transformers`) and [`compile`].
///
/// # Errors
///
/// It is up to you to format runtime errors (e.g using [`error::display_ansi_error`]
/// or [`error::display_html_error`]).
///
/// For example, to display the error using ANSI escape codes (to be used in shells) you can
/// call it like this:
///
/// ```
/// use pochoir::{compile, Context, Transformers, ComponentFile};
///
/// let source = "<h1>A title</h1>{{ [1, 2]['abc'] }}";
/// let compiled = pochoir::transform_and_compile("index", &mut Context::new(), |name| {
///     Ok(ComponentFile::new_inline(source))
/// }, &mut Transformers::new()).map_err(|e| {
///     // Runtime error formatting happens here, it uses
///     // `pochoir::common::Spanned::component_name` to get
///     // the name of the component which raised the error and
///     // fetches it from the provider to get the text source of
///     // the component
///     pochoir::error::display_ansi_error(
///         &e,
///         &source,
///     )
/// });
///
/// assert_eq!(
///     compiled,
///     Err("\u{1b}[1;31merror\u{1b}[0m\u{1b}[1m:\u{1b}[0m \u{1b}[1marray cannot be indexed by String, the index must be a positive number or a range\u{1b}[0m\n   inline:1:27\n\n \u{1b}[1;34m1\u{1b}[0m \u{1b}[1;34m|\u{1b}[0m <h1>A title</h1>{{ [1, 2][\u{1b}[1;31m'abc'\u{1b}[0m] }}".to_string())
/// );
/// ```
///
/// [`error::display_ansi_error`]: crate::error::display_ansi_error
/// [`error::display_html_error`]: crate::error::display_html_error
pub fn transform_and_compile<'a, 'b, 'c>(
    default_component_name: &str,
    default_context: &mut Context,
    mut component_resolver: impl FnMut(&str) -> Result<ComponentFile<'b, 'c>>,
    transformers: &mut Transformers,
) -> Result<String> {
    component_resolver(default_component_name).and_then(|file| {
        let mut result = String::new();
        let tree =
            parse_and_tranform(&file.path, default_component_name, &file.data, transformers)?;

        compile_recursive(
            &mut CompilationContext {
                name: default_component_name,
                context: default_context,
                tree: &tree,
                node_ids: tree.root_nodes(),
                slots: &HashMap::new(),
                parent: None,
            },
            &mut component_resolver,
            &mut result,
            Cow::Borrowed(&HashMap::new()),
            transformers,
        )?;

        Ok(result)
    })
}

#[allow(clippy::too_many_lines)]
fn compile_recursive<'a, 'b, 'c, 'd>(
    ctx: &mut CompilationContext<'_, '_, '_, 'a, '_, '_, '_>,
    component_resolver: &mut impl FnMut(&str) -> Result<ComponentFile<'c, 'd>>,
    result: &mut String,
    mut components: Cow<HashMap<String, Tree<'a>>>,
    transformers: &mut Transformers,
) -> Result<()> {
    let mut node_index = 0;

    while node_index < ctx.node_ids.len() {
        let node = ctx.tree.get(ctx.node_ids[node_index]);
        node_index += 1;

        match node.data() {
            Node::Element(el_name, attrs) => {
                if el_name == "slot" {
                    // Slot element
                    let slot_name_blocks = node
                        .attr_spanned("name")
                        .unwrap_or(vec![Spanned::new(TemplateBlock::text("default"))]);

                    let mut slot_name = String::new();
                    render_template_block(
                        ctx,
                        component_resolver,
                        &mut node_index,
                        &slot_name_blocks,
                        &mut slot_name,
                        &components,
                        transformers,
                    )?;

                    if let Some(node_ids) = ctx.slots.get(&*slot_name) {
                        // Add all the other attributes passed to the <slot> element to the
                        // context (for scoped slots)
                        let mut context = ctx
                            .parent
                            .expect("slot element should be used in a component")
                            .context
                            .clone();

                        // Insert global variables
                        for (key, val) in ctx
                            .context
                            .iter()
                            .filter(|(k, _)| ctx.context.is_inherited(&**k).unwrap())
                        {
                            context.insert(key, val.clone());
                        }

                        for (key, val_blocks) in attrs {
                            if **key == "slot" {
                                // Ignore the `slot` and `name` attributes
                                continue;
                            }

                            let evaluated_value = compile_template_block(
                                ctx,
                                component_resolver,
                                &mut node_index,
                                val_blocks,
                                true,
                                &components,
                                transformers,
                            )?;

                            context.insert(&***key, evaluated_value);
                        }

                        let mut rendered = String::new();
                        compile_recursive(
                            &mut CompilationContext {
                                name: ctx
                                    .parent
                                    .expect("slot element should be used in a component")
                                    .name,
                                context: &mut context,
                                tree: ctx
                                    .parent
                                    .expect("slot element should be used in a component")
                                    .tree,
                                node_ids: node_ids.clone(),
                                parent: ctx
                                    .parent
                                    .expect("slot element should be used in a component")
                                    .parent,
                                slots: ctx
                                    .parent
                                    .expect("slot element should be used in a component")
                                    .slots,
                            },
                            component_resolver,
                            &mut rendered,
                            Cow::Borrowed(&*components),
                            transformers,
                        )?;

                        if rendered.is_empty() {
                            // Try to use the default content
                            //
                            // Default slot content must not have nested <slot> so it is safe to
                            // make an empty list of slots
                            compile_recursive(
                                &mut CompilationContext {
                                    name: ctx.name,
                                    context: ctx.context,
                                    tree: ctx.tree,
                                    node_ids: node.children_id(),
                                    parent: ctx.parent,
                                    slots: &HashMap::new(),
                                },
                                component_resolver,
                                result,
                                Cow::Borrowed(&*components),
                                transformers,
                            )?;
                        } else {
                            result.push_str(&rendered);
                        }
                    } else {
                        // Try to use the default content
                        //
                        // Default slot content must not have nested <slot> so it is safe to
                        // make an empty list of slots
                        compile_recursive(
                            &mut CompilationContext {
                                name: ctx.name,
                                context: ctx.context,
                                tree: ctx.tree,
                                node_ids: node.children_id(),
                                parent: ctx.parent,
                                slots: &HashMap::new(),
                            },
                            component_resolver,
                            result,
                            Cow::Borrowed(&*components),
                            transformers,
                        )?;
                    }
                } else if el_name == "template" {
                    let name_blocks = node
                        .attr_spanned("name")
                        .unwrap_or(vec![Spanned::new(TemplateBlock::text("default"))]);

                    let mut name = String::new();
                    render_template_block(
                        ctx,
                        component_resolver,
                        &mut node_index,
                        &name_blocks,
                        &mut name,
                        &components,
                        transformers,
                    )?;

                    components.to_mut().insert(name, node.sub_tree());
                } else if is_custom_element(el_name) {
                    // Component element
                    if el_name == ctx.name {
                        let spanned_data = node.spanned_data();
                        return Err(SpannedWithComponent::new(Error::CyclicComponent {
                            name: ctx.name.to_string(),
                        })
                        .with_span(spanned_data.span().clone())
                        .with_file_path(spanned_data.file_path())
                        .with_component_name(ctx.name));
                    }

                    // To extend the lifetime of the component source in case it was resolved from
                    // the closure, we need to store it in an Option<T> before the closure is
                    // called.
                    let mut resolved_component_file = None;
                    let component_result = components
                        .get(&**el_name)
                        .ok_or(())
                        .map(Cow::Borrowed)
                        .or_else(|_| {
                            let file = component_resolver(el_name)?;
                            resolved_component_file = Some(file);
                            parse_and_tranform(
                                &resolved_component_file.as_ref().unwrap().path,
                                el_name,
                                &resolved_component_file.as_ref().unwrap().data,
                                transformers,
                            )
                            .map(Cow::Owned)
                        });

                    match component_result {
                        Ok(tree) => {
                            // Slots
                            let mut slots: HashMap<Cow<str>, Vec<TreeRefId>> = HashMap::new();

                            for child_id in node.children_id() {
                                let child = ctx.tree.get(child_id);

                                let slot_name_blocks = child
                                    .attr_spanned("slot")
                                    .unwrap_or(vec![Spanned::new(TemplateBlock::text("default"))]);

                                let mut slot_name = String::new();
                                render_template_block(
                                    ctx,
                                    component_resolver,
                                    &mut node_index,
                                    &slot_name_blocks,
                                    &mut slot_name,
                                    &components,
                                    transformers,
                                )?;

                                if let Some(children) = slots.get_mut(&*slot_name) {
                                    children.push(child_id);
                                } else {
                                    slots.insert(Cow::Owned(slot_name), vec![child_id]);
                                }
                            }

                            // Attributes
                            let mut context = Context::new();

                            for (key, val_blocks) in attrs {
                                if **key == "slot" {
                                    // Ignore the `slot` attribute
                                    continue;
                                }

                                let evaluated_value = compile_template_block(
                                    ctx,
                                    component_resolver,
                                    &mut node_index,
                                    val_blocks,
                                    true,
                                    &components,
                                    transformers,
                                )?;
                                context.insert(key.to_case(Case::Snake), evaluated_value);
                            }

                            // Insert global variables
                            for (key, val) in ctx
                                .context
                                .iter()
                                .filter(|(k, _)| ctx.context.is_inherited(&**k).unwrap())
                            {
                                context.insert(key, val.clone());
                            }

                            compile_recursive(
                                &mut CompilationContext {
                                    name: el_name,
                                    context: &mut context,
                                    tree: &tree,
                                    node_ids: tree.root_nodes(),
                                    slots: &slots,
                                    parent: Some(ctx),
                                },
                                component_resolver,
                                result,
                                Cow::Borrowed(&*components),
                                transformers,
                            )?;
                        }
                        Err(e) if matches!(**e, Error::ComponentNotFound { .. }) => {
                            if let Error::ComponentNotFound { name } = (**e).clone() {
                                // Fix span of component not found error
                                let spanned_data = node.spanned_data();
                                return Err(SpannedWithComponent::new(Error::ComponentNotFound {
                                    name,
                                })
                                .with_span(spanned_data.span().clone())
                                .with_file_path(spanned_data.file_path())
                                .with_component_name(ctx.name));
                            }

                            unreachable!();
                        }
                        Err(e) => {
                            // An error occured in a component
                            return Err(e);
                        }
                    }
                } else {
                    // Not component element
                    write!(result, "<{el_name}").expect("writing to a String can't fail");

                    // Attributes
                    for (key, val_blocks) in attrs {
                        if **key == "slot" {
                            // Ignore the `slot` attribute
                            continue;
                        }

                        // We don't care about HTML elements (like slots or components) in attribute values, so we can
                        // just use the render function of the template engine instead of the
                        // render_template_block function
                        let attr_val =
                            crate::template_engine::render_template(val_blocks, ctx.context)
                                .auto_error_with_name(ctx.name)?;

                        if attr_val.is_empty() {
                            write!(result, " {}", **key).expect("writing to a String can't fail");
                        } else {
                            write!(result, " {}=\"{}\"", **key, attr_val)
                                .expect("writing to a String can't fail");
                        }
                    }

                    write!(result, ">").expect("writing to a String can't fail");

                    // All nodes in the same component are considered as part of the same
                    // compilation context
                    compile_recursive(
                        &mut CompilationContext {
                            name: ctx.name,
                            context: ctx.context,
                            tree: ctx.tree,
                            node_ids: node.children_id(),
                            slots: ctx.slots,
                            parent: ctx.parent,
                        },
                        component_resolver,
                        result,
                        Cow::Borrowed(&*components),
                        transformers,
                    )?;

                    if !EMPTY_HTML_ELEMENTS.contains(&&**el_name) {
                        write!(result, "</{el_name}>").expect("writing to a String can't fail");
                    }
                }
            }
            Node::Comment(_) => (),
            Node::Doctype(doctype) => {
                write!(result, "<!DOCTYPE {doctype}>").expect("writing to a String can't fail");
            }
            Node::TemplateBlock(block) => {
                render_template_block(
                    ctx,
                    component_resolver,
                    &mut node_index,
                    &[Spanned::new(block)
                        .with_span(node.spanned_data().span().clone())
                        .with_file_path(ctx.tree.file_path())],
                    result,
                    &components,
                    transformers,
                )?;
            }
            Node::Root => unreachable!(),
        }
    }

    Ok(())
}
