//! Module defining the main [`enum@Error`] type, the [`AutoError`] trait used for compatibility
//! between the main error type and the errors for sibling crates and some functions to nicely
//! display the errors ([`display_html_error`], [`display_ansi_error`]).
use crate::common::Spanned;
use ansi_term::{Color, Style};
use pochoir_common::span::SpanPosition;
use std::{
    borrow::Cow,
    error, fmt,
    ops::{Deref, DerefMut, Range},
    path::Path,
    result,
};
use thiserror::Error;

/// An alias to an [`std::result::Result`] with [`SpannedWithComponent`](`SpannedWithComponent`)
/// and an inner [`enum@Error`].
pub type Result<T> = std::result::Result<T, SpannedWithComponent<Error>>;

/// Error enumeration describing all the possible errors happening during the compilation of a
/// template or component.
#[derive(Error, Debug, PartialEq, Eq, Clone)]
pub enum Error {
    /// The component with the name `name` imports itself in its content, it creates a cycle and is
    /// not accepted by the compiler.
    #[error("component {name:?} is importing itself resulting in a cycle")]
    CyclicComponent {
        /// The name of the component importing itself.
        name: String,
    },

    /// A component with the name `name` is not found.
    #[error("component {name:?} not found")]
    ComponentNotFound {
        /// The name of the component which is not found.
        name: String,
    },

    /// An unknown statement was used. Only known statements (if/else/elif/endif/for/endfor/let)
    /// are accepted by the compiler. Just the first word of the statement value is given in the
    /// `stmt` field.
    #[error("unknown statement {stmt:?}")]
    UnknownStatement {
        /// The first word of the statement value.
        ///
        /// For example, if you write a statement `{% switch sth %}`, this field will contain the
        /// value `switch`.
        stmt: String,
    },

    /// An unbounded range is used in a for loop, e.g ..3 or 4..
    ///
    /// It is not accepted because a for loop is not allowed to loop infinitely.
    #[error("unbounded ranges are forbidden in `for` loops, if you want to index an array you can use its length as the end bound")]
    UnboundedRangeInForLoop,

    /// An `elif` statement is used after an `else` statement.
    #[error("an `elif` statement cannot be used after an `else` statement, you may have forgotten an `endif` statement")]
    ElifAfterElse,

    /// An error happening in the template engine.
    #[error(transparent)]
    TemplateError(#[from] crate::template_engine::Error),

    /// An error happening in the custom language parser/interpreter.
    #[error(transparent)]
    LangError(#[from] crate::lang::Error),

    /// An error happening in the HTML parser.
    #[error(transparent)]
    ParserError(#[from] crate::parser::Error),

    /// An error happening in the stream parser.
    #[error(transparent)]
    StreamParserError(#[from] crate::common::Error),
}

/// A [`Spanned`] storing the component name on top of the original span and file path.
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct SpannedWithComponent<T> {
    original_spanned: Box<Spanned<T>>,
    component_name: String,
}

impl<T> SpannedWithComponent<T> {
    /// Create a new [`SpannedWithComponent`] with the given inner value.
    pub fn new(node: T) -> Self {
        Self {
            original_spanned: Box::new(Spanned::new(node)),
            component_name: String::default(),
        }
    }

    /// Set the component name of the [`SpannedWithComponent`] using the builder pattern.
    #[must_use]
    pub fn with_component_name<N: Into<String>>(mut self, component_name: N) -> Self {
        self.component_name = component_name.into();
        self
    }

    /// Set the component name of the [`SpannedWithComponent`].
    pub fn set_component_name<N: Into<String>>(&mut self, component_name: N) {
        self.component_name = component_name.into();
    }

    /// Get the component name of the [`SpannedWithComponent`].
    pub fn component_name(&self) -> &str {
        &self.component_name
    }

    /// Set the span of the inner [`Spanned`].
    #[must_use]
    pub fn with_span(mut self, span: Range<usize>) -> Self {
        self.original_spanned.set_span(span);
        self
    }

    /// Set the file path of the inner [`Spanned`].
    #[must_use]
    pub fn with_file_path<P: AsRef<Path>>(mut self, file_path: P) -> Self {
        self.original_spanned.set_file_path(file_path.as_ref());
        self
    }
}

impl<T> From<Spanned<T>> for SpannedWithComponent<T> {
    fn from(original_spanned: Spanned<T>) -> Self {
        Self {
            original_spanned: Box::new(original_spanned),
            component_name: String::default(),
        }
    }
}

impl<T> Deref for SpannedWithComponent<T> {
    type Target = Spanned<T>;

    fn deref(&self) -> &Self::Target {
        &self.original_spanned
    }
}

impl<T> DerefMut for SpannedWithComponent<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.original_spanned
    }
}

impl<T: fmt::Display> fmt::Display for SpannedWithComponent<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.original_spanned.fmt(f)
    }
}

impl<T: error::Error> error::Error for SpannedWithComponent<T> {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        self.original_spanned.source()
    }
}

/// A trait used to convert errors coming from sibling crates (`pochoir-template-engine`,
/// `pochoir-lang`, …) to the single [`enum@Error`] structure of the main crate.
///
/// You should use this trait if you use a function from a sibling crate with a function from the
/// main `pochoir` crate and want to return a single error type.
///
/// You should not have to implement this trait yourself, except if you are a core developer of
/// `pochoir`.
pub trait AutoError<T>
where
    Self: Sized,
{
    /// Convert the error to a `pochoir` [`enum@Error`], generally by using the dedicated field in
    /// the enumeration.
    fn auto_error(self) -> T;
}

pub(crate) trait AutoErrorOffset<T>
where
    Self: Sized,
{
    fn auto_error_offset(self, file_offset: usize) -> T;
}

impl<T> AutoError<Result<T>> for result::Result<T, Spanned<crate::parser::Error>> {
    fn auto_error(self) -> Result<T> {
        self.map_err(|e| SpannedWithComponent::from(e.map_spanned(Error::ParserError)))
    }
}

impl<T> AutoErrorOffset<result::Result<T, Spanned<Error>>>
    for result::Result<T, Spanned<crate::common::Error>>
{
    fn auto_error_offset(self, file_offset: usize) -> result::Result<T, Spanned<Error>> {
        self.map_err(|e| {
            let span = e.span().clone();
            e.map_spanned(Error::StreamParserError)
                .with_span(span.start + file_offset..span.end + file_offset)
        })
    }
}

impl<T> AutoError<result::Result<T, Spanned<Error>>>
    for result::Result<T, Spanned<crate::lang::Error>>
{
    fn auto_error(self) -> result::Result<T, Spanned<Error>> {
        self.map_err(|e| e.map_spanned(Error::LangError))
    }
}

impl<T> AutoError<result::Result<T, Spanned<Error>>>
    for result::Result<T, Spanned<crate::template_engine::Error>>
{
    fn auto_error(self) -> result::Result<T, Spanned<Error>> {
        self.map_err(|e| e.map_spanned(Error::TemplateError))
    }
}

pub(crate) trait AutoErrorWithName<T>
where
    Self: Sized,
{
    fn auto_error_with_name<N: Into<String>>(self, component_name: N) -> T;
}

pub(crate) trait AutoErrorWithNameOffset<T>
where
    Self: Sized,
{
    fn auto_error_with_name_offset<N: Into<String>>(
        self,
        component_name: N,
        file_offset: usize,
    ) -> T;
}

impl<T> AutoErrorWithName<Result<T>> for result::Result<T, Spanned<crate::parser::Error>> {
    fn auto_error_with_name<N: Into<String>>(self, component_name: N) -> Result<T> {
        self.map_err(|e| {
            SpannedWithComponent::from(e.map_spanned(Error::ParserError))
                .with_component_name(component_name)
        })
    }
}

impl<T> AutoErrorWithNameOffset<Result<T>> for result::Result<T, Spanned<crate::common::Error>> {
    fn auto_error_with_name_offset<N: Into<String>>(
        self,
        component_name: N,
        file_offset: usize,
    ) -> Result<T> {
        self.map_err(|e| {
            let span = e.span().clone();
            SpannedWithComponent::from(e.map_spanned(Error::StreamParserError))
                .with_span(span.start + file_offset..span.end + file_offset)
                .with_component_name(component_name)
        })
    }
}

impl<T> AutoErrorWithName<Result<T>> for result::Result<T, Spanned<crate::lang::Error>> {
    fn auto_error_with_name<N: Into<String>>(self, component_name: N) -> Result<T> {
        self.map_err(|e| {
            SpannedWithComponent::from(e.map_spanned(Error::LangError))
                .with_component_name(component_name)
        })
    }
}

impl<T> AutoErrorWithName<Result<T>> for result::Result<T, Spanned<crate::template_engine::Error>> {
    fn auto_error_with_name<N: Into<String>>(self, component_name: N) -> Result<T> {
        self.map_err(|e| {
            SpannedWithComponent::from(e.map_spanned(Error::TemplateError))
                .with_component_name(component_name)
        })
    }
}

/// A shortcut returning an [`Error::ComponentNotFound`] indicating that the component is
/// not found.
///
/// Useful in the closure API where it should be returned when the component having the name given
/// as argument does not exist.
pub fn component_not_found<N: Into<String>>(name: N) -> SpannedWithComponent<Error> {
    SpannedWithComponent::new(Error::ComponentNotFound { name: name.into() })
}

/// Display a structure implementing the [`std::error::Error`] trait wrapped in a [`Spanned`] to an
/// HTML-formatted string.
///
/// If the source is too short for the span to be contained in it, `(no source)` will be displayed
/// instead of the source content and if the file path is not set, `(no path)` will be displayed
/// instead.
///
/// Each formatted error is like:
///
/// <style>
/// pre#no-styles {
///   background-color: initial;
///   padding: 0px;
///   line-height: initial;
///   padding: 16px;
///   border: 2px solid #D5D5D5;
/// }
/// </style>
///
/// <pre id="no-styles" style="font-family: monospace, sans-serif;"><span style="color: red; font-weight: 600;">error</span><span style="font-weight: 600;">: component "does-not-exist" not found</span>
/// <span style="margin-left: 2rem; color: #666; font-size: 0.9em;">./index.html:9:3</span>
///
/// <code style="background-color: #F5F5F5; color: black; width: max-content; display: inline-block; padding: 14px; line-height: 1.5;"> 7 |   &lt;li&gt;How to fix a coffee maker&lt;/li&gt;
///  8 |   &lt;li&gt;How to make an API using &lt;code&gt;Rust&lt;/code&gt; and &lt;code&gt;actix-web&lt;/code&gt;&lt;/li&gt;
///  9 |   <span style="font-weight: 900; color: red;">&lt;does-not-exist /&gt;</span>
/// 10 | &lt;/ul&gt;</code></pre>
pub fn display_html_error<E: std::error::Error>(error: &Spanned<E>, source: &str) -> String {
    let (source_context, pos) = if error.is_dummy_span() {
        (
            Cow::Borrowed("(no source)"),
            SpanPosition { row: 0, col: 0 },
        )
    } else if let (Some(section), Some(pos)) =
        (source.get(error.span().clone()), error.get_position(source))
    {
        let context_row_start = pos.row.saturating_sub(3);

        #[allow(clippy::cast_sign_loss)]
        #[allow(clippy::cast_possible_wrap)]
        let context_row_middle = if pos.row == context_row_start {
            0
        } else {
            2 - (context_row_start as isize - (pos.row as isize - 3isize)) as usize
        };
        let context_row_end = context_row_start + 4;

        // Count the maximum number of digits used for line numbers, useful for padding
        let max_digits = ((context_row_end + 1).checked_ilog10().unwrap_or(0) + 1) as usize;

        let lines = source
            .lines()
            .skip(context_row_start)
            .take(5)
            .enumerate()
            .map(|(i, l)| {
                if i == context_row_middle {
                    // TODO: Display error when current error span is on several lines
                    let remaining = (pos.col + section.len() + usize::from(pos.col == 0))
                        .checked_sub(1)
                        .unwrap_or(pos.col + section.len());

                    Cow::Owned(format!(
                        r#"{}{} | {}<span style="font-weight: 900; color: red;">{}</span>{}"#,
                        " ".repeat(
                            max_digits
                                - ((context_row_start + i + 1).checked_ilog10().unwrap_or(0) + 1)
                                    as usize
                        ),
                        context_row_start + i + 1,
                        crate::template_engine::Escaping::Html
                            .escape(&l[..pos.col.checked_sub(1).unwrap_or(pos.col)]),
                        if remaining > l.len() {
                            Cow::Borrowed("")
                        } else {
                            crate::template_engine::Escaping::Html.escape(section)
                        },
                        if remaining > l.len() {
                            Cow::Borrowed(r#"<span style="color: red;">\u{2588}</span>"#)
                        } else {
                            crate::template_engine::Escaping::Html.escape(&l[remaining..])
                        },
                    ))
                } else {
                    crate::template_engine::Escaping::Html.escape(format!(
                        "{}{} | {}",
                        " ".repeat(
                            max_digits
                                - ((context_row_start + i + 1).checked_ilog10().unwrap_or(0) + 1)
                                    as usize
                        ),
                        context_row_start + i + 1,
                        l
                    ))
                }
            })
            .collect::<Vec<Cow<str>>>();

        (Cow::Owned(lines.join("\n")), pos)
    } else {
        // Source is too short
        (
            Cow::Borrowed("(no source)"),
            SpanPosition { row: 0, col: 0 },
        )
    };

    let file_path = error.file_path().to_string_lossy();
    format!(
        r#"<pre style="font-family: monospace, sans-serif;"><span style="color: red; font-weight: 600;">error</span><span style="font-weight: 600;">: {}</span>
<span style="margin-left: 2rem; color: #666; font-size: 0.9em;">{}:{}:{}</span>

<code style="background-color: #F5F5F5; width: max-content; display: inline-block; padding: 14px; line-height: 1.5;">{}</code></pre>"#,
        crate::template_engine::Escaping::Html.escape(error.to_string()),
        if file_path.is_empty() {
            Cow::Borrowed("(no path)")
        } else {
            file_path
        },
        pos.row,
        pos.col,
        source_context,
    )
}

/// Display a structure implementing the [`std::error::Error`] trait wrapped in a [`Spanned`] to an
/// ANSI-formatted string.
///
/// If the source is too short for the span to be contained in it, `(no source)` will be displayed
/// instead of the source content and if the file path is not set, `(no path)` will be displayed
/// instead.
///
/// Each formatted error is like:
///
/// <style>
/// pre#no-styles {
///   background-color: initial;
///   padding: 0px;
///   line-height: initial;
///   padding: 16px;
///   border: 2px solid #D5D5D5;
/// }
///
/// pre#no-styles .ansi-red-fg {
///   color: red;
/// }
///
/// pre#no-styles .ansi-blue-fg {
///   color: blue;
/// }
/// </style>
///
/// <pre id="no-styles"><span style="font-weight:bold" class="ansi-red-fg">error</span><span style="font-weight:bold">:</span> <span style="font-weight:bold">component "does-not-exist" not found</span>
///    ./index.html:9:3
///
///   <span style="font-weight:bold" class="ansi-blue-fg">7</span> <span style="font-weight:bold" class="ansi-blue-fg">|</span>   &lt;li&gt;How to fix a coffee maker&lt;/li&gt;
///   <span style="font-weight:bold" class="ansi-blue-fg">8</span> <span style="font-weight:bold" class="ansi-blue-fg">|</span>   &lt;li&gt;How to make an API using &lt;code&gt;Rust&lt;/code&gt; and &lt;code&gt;actix-web&lt;/code&gt;&lt;/li&gt;
///   <span style="font-weight:bold" class="ansi-blue-fg">9</span> <span style="font-weight:bold" class="ansi-blue-fg">|</span>   <span style="font-weight:bold" class="ansi-red-fg">&lt;does-not-exist /&gt;</span>
///  <span style="font-weight:bold" class="ansi-blue-fg">10</span> <span style="font-weight:bold" class="ansi-blue-fg">|</span> &lt;/ul&gt;
/// </pre>
pub fn display_ansi_error<E: std::error::Error>(error: &Spanned<E>, source: &str) -> String {
    let (source_context, pos) = if error.is_dummy_span() {
        (
            Cow::Borrowed("(no source)"),
            SpanPosition { row: 0, col: 0 },
        )
    } else if let (Some(section), Some(pos)) =
        (source.get(error.span().clone()), error.get_position(source))
    {
        let context_row_start = pos.row.saturating_sub(3);

        #[allow(clippy::cast_sign_loss)]
        #[allow(clippy::cast_possible_wrap)]
        let context_row_middle = if pos.row == context_row_start {
            0
        } else {
            2 - (context_row_start as isize - (pos.row as isize - 3isize)) as usize
        };
        let context_row_end = context_row_start + 4;

        // Count the maximum number of digits used for line numbers, useful for padding
        let max_digits = ((context_row_end + 1).checked_ilog10().unwrap_or(0) + 1) as usize;

        let lines = source
            .lines()
            .skip(context_row_start)
            .take(5)
            .enumerate()
            .map(|(i, l)| {
                if i == context_row_middle {
                    // TODO: Display error when current error span is on several lines
                    let remaining = (pos.col + section.len() + usize::from(pos.col == 0))
                        .checked_sub(1)
                        .unwrap_or(pos.col + section.len());

                    format!(
                        r#" {}{} {} {}{}{}"#,
                        " ".repeat(
                            max_digits
                                - ((context_row_start + i + 1).checked_ilog10().unwrap_or(0) + 1)
                                    as usize
                        ),
                        Color::Blue
                            .bold()
                            .paint((context_row_start + i + 1).to_string()),
                        Color::Blue.bold().paint("|"),
                        &l[..pos.col.checked_sub(1).unwrap_or(pos.col)],
                        if remaining > l.len() {
                            Cow::Borrowed("")
                        } else {
                            Cow::Owned(Color::Red.bold().paint(section).to_string())
                        },
                        if remaining > l.len() {
                            Cow::Owned(Color::Red.paint("\u{2588}").to_string())
                        } else {
                            Cow::Borrowed(&l[remaining..])
                        },
                    )
                } else {
                    format!(
                        " {}{} {} {}",
                        " ".repeat(
                            max_digits
                                - ((context_row_start + i + 1).checked_ilog10().unwrap_or(0) + 1)
                                    as usize
                        ),
                        Color::Blue
                            .bold()
                            .paint((context_row_start + i + 1).to_string()),
                        Color::Blue.bold().paint("|"),
                        l
                    )
                }
            })
            .collect::<Vec<String>>();

        (Cow::Owned(lines.join("\n")), pos)
    } else {
        // Source is too short
        (
            Cow::Borrowed("(no source)"),
            SpanPosition { row: 0, col: 0 },
        )
    };

    let file_path = error.file_path().to_string_lossy();
    format!(
        "{}{} {}\n   {}:{}:{}\n\n{source_context}",
        Color::Red.bold().paint("error"),
        Style::new().bold().paint(":"),
        Style::new().bold().paint(error.to_string()),
        if file_path.is_empty() {
            Cow::Borrowed("(no path)")
        } else {
            file_path
        },
        pos.row,
        pos.col,
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn source_too_short() {
        assert_eq!(
            display_html_error(
                &Spanned::new(Error::ElifAfterElse).with_span(0..120),
                "hello world"
            ),
            r#"<pre style="font-family: monospace, sans-serif;"><span style="color: red; font-weight: 600;">error</span><span style="font-weight: 600;">: an `elif` statement cannot be used after an `else` statement, you may have forgotten an `endif` statement</span>
<span style="margin-left: 2rem; color: #666; font-size: 0.9em;">(no path):0:0</span>

<code style="background-color: #F5F5F5; width: max-content; display: inline-block; padding: 14px; line-height: 1.5;">(no source)</code></pre>"#
        );

        assert_eq!(
            display_ansi_error(&Spanned::new(Error::ElifAfterElse).with_span(0..120), "hello world"),
            "\u{1b}[1;31merror\u{1b}[0m\u{1b}[1m:\u{1b}[0m \u{1b}[1man `elif` statement cannot be used after an `else` statement, you may have forgotten an `endif` statement\u{1b}[0m
   (no path):0:0

(no source)"
        );
    }
}
