#![no_main]

use libfuzzer_sys::fuzz_target;
use pochoir::{Context, TemplateCompiler};
use std::borrow::Cow;

fuzz_target!(|data: &[u8]| {
    if let Ok(s) = std::str::from_utf8(data) {
        let mut compiler = TemplateCompiler::new();
        if compiler
            .insert_template("template.html", Cow::Borrowed(s))
            .is_ok()
        {
            if let Ok(compiled) = compiler.compile("template.html", &Context::new()) {
                let _ = compiled.render();
            }
        }
    }
});
