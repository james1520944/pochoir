//! CSS enhancer using the awesome [lightningcss](https://lightningcss.dev/) crate.
//!
//! Support:
//!
//! - Scoped CSS
//! - CSS minification
//! - CSS [nesting](https://www.w3.org/TR/css-nesting-1/) and custom [@media queries](https://drafts.csswg.org/mediaqueries-5/#custom-mq)
//! - CSS autoprefixer and polyfiller (supports using modern CSS and generates CSS compatible with
//! the chosen browsers)
//!
//! This plugin has two ways of use: either you can enhance the CSS of a single component and write
//! a single `<style>` element containing the generated CSS in it (see [`EnhancedCss`]), or you can bundle all the enhanced CSS into
//! a single string that you can use as you wish (see [`EnhancedCssBundler`]).
//!
//! In either way, you need to use the `enhanced` attribute on any `<style>` element that you want to be enhanced. You can also add a `.browserslistrc` file to configure the target browsers of the
//! generated CSS (see [the other ways to configure the targets](https://docs.rs/lightningcss/1.0.0-alpha.44/lightningcss/targets/struct.Browsers.html#method.load_browserslist)). The CSS will also be scoped to the component defining it to avoid selector clashing.
//!
//! Note that when using this transformer, the component names **must be unique**, otherwise the
//! scoped CSS generated will have conflicts.
use lightningcss::{
    rules::CssRule,
    selector,
    stylesheet::{MinifyOptions, ParserFlags, ParserOptions, PrinterOptions, StyleSheet},
    targets::{Browsers, Features, Targets},
    values::ident::Ident,
};
use std::{
    borrow::Cow,
    collections::hash_map::DefaultHasher,
    hash::{Hash, Hasher},
    path::Path,
};

use pochoir::{
    common::Spanned,
    parser::{Attrs, Node, Tree, TreeRef, TreeRefId},
    template_engine::{Escaping, TemplateBlock},
    transformers::Transformer,
    TransformerResult,
};

fn do_scoped_css(component_name: &str, tree: &mut Tree, stylesheet: &mut StyleSheet) {
    // Generated IDs should have at most 8 digits
    const MAX_DIGITS: usize = 8;

    // Generate a unique component ID by hashing the component name
    let mut hasher = DefaultHasher::new();
    component_name.hash(&mut hasher);

    let unique_id = format!("{:x}", hasher.finish());
    let unique_id_attr = format!("data-p-{}", &unique_id[..MAX_DIGITS]);

    // Add this ID as a data attribute on all elements of the component
    for id in tree.all_nodes() {
        let node = tree.get(id);

        if node.name().map_or(false, |el_name| {
            !["html", "body", "style", "script"].contains(&&*el_name)
        }) && node.closest("head").is_none()
        {
            tree.get_mut(id)
                .set_attr(unique_id_attr.clone(), None::<String>, Escaping::None);
        }
    }

    // Add this data attribute to all CSS selectors of the stylesheet (containing the concatenated
    // CSS of all <style> elements of the component)
    let local_name = Ident::from(unique_id_attr);

    for rule in &mut stylesheet.rules.0 {
        if let CssRule::Style(ref mut style_rule) = rule {
            for selector in &mut style_rule.selectors.0 {
                selector.append(selector::Component::AttributeInNoNamespaceExists {
                    local_name: local_name.clone(),
                    local_name_lower: local_name.clone(),
                });
            }
        }
    }
}

/// Enhance the CSS of a single component and write a single `<style>` element
/// containing the generated CSS in it.
///
/// # Example
///
/// ```no_run
/// use pochoir::{lang::Context, ComponentFile, transformers::Transformers};
/// use pochoir_extra::EnhancedCss;
///
/// let source = r#"
/// <main>
///   <a href="/"></a>
/// </main>
///
/// {# Don't forget the `enhanced` attribute #}
/// <style enhanced>
/// main {
///   background-color: rebeccapurple;
///
///   & a {
///     color: aquamarine;
///   }
/// }
/// </style>"#;
/// let file_path = std::path::Path::new("index.html");
///
/// // 1. Add the `EnhancedCss` structure as a transformer
/// let mut transformers = Transformers::new()
///     .with_transformer(EnhancedCss::new());
///
/// // 2. Use `pochoir::transform_and_compile` with a mutable reference
/// // to the transformers as last argument
/// assert_eq!(
///     pochoir::transform_and_compile("index", &mut Context::new(), |name| {
///         Ok(ComponentFile::new(file_path, source))
///     }, &mut transformers),
///     Ok(r#"
/// <main data-p-fd1ea890>
///   <a href="/" data-p-fd1ea890></a>
/// </main>
///
///
/// <style>main[data-p-fd1ea890]{background-color:#639}main[data-p-fd1ea890] a{color:#7fffd4}</style>"#.to_string()),
/// );
/// ```
#[derive(Debug)]
pub struct EnhancedCss {
    style_source: String,
    include_css_features: Features,
    exclude_css_features: Features,
}

impl EnhancedCss {
    /// Create a new [`EnhancedCss`].
    pub fn new() -> Self {
        Self {
            style_source: String::new(),
            include_css_features: Features::default(),
            exclude_css_features: Features::default(),
        }
    }

    /// Define the CSS features that you want your transpiled CSS code to use by including them.
    ///
    /// See [`lightningcss::targets::Features`].
    #[must_use]
    pub fn include_css_features(mut self, features: Features) -> Self {
        self.include_css_features = features;
        self
    }

    /// Define the CSS features that you want your transpiled CSS code to use by excluding the ones
    /// you don't want.
    ///
    /// See [`lightningcss::targets::Features`].
    #[must_use]
    pub fn exclude_css_features(mut self, features: Features) -> Self {
        self.exclude_css_features = features;
        self
    }
}

impl Transformer for EnhancedCss {
    fn on_after_element(&mut self, tree: &mut Tree, id: TreeRefId) -> TransformerResult {
        let el = tree.get(id);

        if el.name().unwrap() == "style" && el.attr("enhanced").is_some() {
            self.style_source
                .push_str(&el.children().iter().map(TreeRef::text).collect::<String>());
            tree.get_mut(id).remove();
        }

        Ok(())
    }

    fn on_tree_parsed(
        &mut self,
        tree: &mut Tree,
        component_name: &str,
        file_path: &Path,
    ) -> TransformerResult {
        let targets = Targets {
            browsers: Browsers::load_browserslist()?,
            include: self.include_css_features,
            exclude: self.exclude_css_features,
        };

        let mut stylesheet = StyleSheet::parse(
            &self.style_source,
            ParserOptions {
                filename: file_path.to_string_lossy().into_owned(),
                flags: ParserFlags::NESTING | ParserFlags::CUSTOM_MEDIA,
                ..Default::default()
            },
        )
        .map_err(|e| e.to_string())?;

        stylesheet.minify(MinifyOptions {
            targets,
            ..Default::default()
        })?;

        do_scoped_css(component_name, tree, &mut stylesheet);

        let enhanced_css = stylesheet
            .to_css(PrinterOptions {
                minify: true,
                targets,
                ..Default::default()
            })?
            .code;

        // Add <style> node as a child of <head> if possible or as the last child
        let parent = tree.select("head").unwrap_or(TreeRefId::Root);
        let style_id = tree.insert(
            parent,
            Spanned::new(Node::Element(Cow::Borrowed("style"), Attrs::new())),
        );
        let _text_id = tree.insert(
            style_id,
            Spanned::new(Node::TemplateBlock(TemplateBlock::RawText(Cow::Owned(
                enhanced_css,
            )))),
        );

        drop(stylesheet);

        // `stylesheet` is now dropped, we can reset the source
        self.style_source = String::new();

        Ok(())
    }
}

impl Default for EnhancedCss {
    fn default() -> Self {
        Self::new()
    }
}

/// Bundle all the enhanced CSS into a single string that you can use as you wish.
///
/// # Example
///
/// ```no_run
/// use pochoir::{lang::Context, ComponentFile, transformers::Transformers};
/// use pochoir_extra::EnhancedCssBundler;
///
/// let index_source = r#"<h1>Index page</h1><my-button />
/// <style enhanced>
/// h1 {
///   font-weight: extrabold;
/// }
/// </style>"#;
/// let my_button_source = r#"<button>Click me!</button>
/// <style enhanced>
/// button:hover {
///   background-color: antiquewhite;
/// }
/// </style>"#;
/// let index_file_path = std::path::Path::new("index.html");
/// let my_button_file_path = std::path::Path::new("my-button.html");
///
/// // 1. Make a buffer which will be filled with the CSS bundle
/// let mut css_bundle = String::new();
///
/// // 2. Add the `EnhancedCssBundler` structure as a transformer and give the buffer to it
/// let mut transformers = Transformers::new()
///     .with_transformer(EnhancedCssBundler::new(&mut css_bundle));
///
/// // 3. Use `pochoir::transform_and_compile` with a mutable reference
/// // to the transformers as last argument
/// let _compiled = pochoir::transform_and_compile("index", &mut Context::new(), |name| {
///     let (file_path, source) = match name {
///         "index" => (index_file_path, index_source),
///         "my-button" => (my_button_file_path, my_button_source),
///         _ => unreachable!(),
///     };

///     Ok(ComponentFile::new(file_path, source))
/// }, &mut transformers);
///
/// // 4. Don't forget to drop the `transformers` before using the bundle because they stored a
/// // mutable reference to it
/// drop(transformers);
/// assert_eq!(css_bundle, "h1[data-p-fd1ea890]{font-weight:extrabold}button:hover[data-p-92dd4f04]{background-color:#faebd7}");
/// ```
#[derive(Debug)]
pub struct EnhancedCssBundler<'a> {
    style_source: String,
    css_bundle: &'a mut String,
    targets: Targets,
}

impl<'a> EnhancedCssBundler<'a> {
    /// Create a new [`EnhancedCssBundler`].
    ///
    /// # Panics
    ///
    /// This function will panic if loading the browserslist failed.
    pub fn new(css_bundle: &'a mut String) -> Self {
        Self::try_new(css_bundle).expect("an error happened when loading browserslist")
    }

    /// Create a new [`EnhancedCssBundler`].
    ///
    /// # Errors
    ///
    /// This function will return a boxed [`std::error::Error`] if loading the browserslist failed.
    pub fn try_new(css_bundle: &'a mut String) -> Result<Self, Box<dyn std::error::Error>> {
        Ok(Self {
            style_source: String::new(),
            css_bundle,
            targets: Targets {
                browsers: Browsers::load_browserslist()?,
                ..Default::default()
            },
        })
    }

    /// Define the CSS features that you want your transpiled CSS code to use by including them.
    ///
    /// See [`lightningcss::targets::Features`].
    #[must_use]
    pub fn include_css_features(mut self, features: Features) -> Self {
        self.targets.include = features;
        self
    }

    /// Define the CSS features that you want your transpiled CSS code to use by excluding the ones
    /// you don't want.
    ///
    /// See [`lightningcss::targets::Features`].
    #[must_use]
    pub fn exclude_css_features(mut self, features: Features) -> Self {
        self.targets.exclude = features;
        self
    }
}

impl<'a> Transformer for EnhancedCssBundler<'a> {
    fn on_after_element(&mut self, tree: &mut Tree, id: TreeRefId) -> TransformerResult {
        let el = tree.get(id);

        if el.name().unwrap() == "style" && el.attr("enhanced").is_some() {
            self.style_source
                .push_str(&el.children().iter().map(TreeRef::text).collect::<String>());
            tree.get_mut(id).remove();
        }

        Ok(())
    }

    fn on_tree_parsed(
        &mut self,
        tree: &mut Tree,
        component_name: &str,
        file_path: &Path,
    ) -> TransformerResult {
        let mut stylesheet = StyleSheet::parse(
            &self.style_source,
            ParserOptions {
                filename: file_path.to_string_lossy().into_owned(),
                flags: ParserFlags::NESTING | ParserFlags::CUSTOM_MEDIA,
                ..Default::default()
            },
        )
        .map_err(|e| e.to_string())?;
        stylesheet.minify(MinifyOptions {
            targets: self.targets,
            ..Default::default()
        })?;

        do_scoped_css(component_name, tree, &mut stylesheet);

        self.css_bundle.push_str(
            &stylesheet
                .to_css(PrinterOptions {
                    minify: true,
                    targets: self.targets,
                    ..Default::default()
                })?
                .code,
        );

        drop(stylesheet);

        // `stylesheet` is now dropped, we can reset the source
        self.style_source = String::new();

        Ok(())
    }
}
