# Changelog

All notable changes to this project will be documented in this file.

## [0.9.0] - 2023-09-02

### Bug Fixes

- Add serde in root Cargo.toml file
- Better support for rendering attributes with an empty value
- Bad index in StreamParser::take_while
- Fix bad index in StreamParser::take_while_peekable
- Fix documentation tests
- Conditions with the member not used producing an error
- Support unary operator and ? operator, fix pochoir_common::StreamParser::next implementation breaking on UTF-8 characters
- Bad precedence in unary operators, inverse precedence level of && and ||
- Parsing namespaced functions
- Hard-coded parent slot used
- Finish new parser: support elements using raw text (like script, style) and support HTML in template expressions and statements
- Improve error messages
- Typo
- Rename FunctionNotDefined -> CannotCallNull, VariableNotDefined -> CannotIndexNull
- Avoid ignoring `name` attributes
- Add file names for all errors during interpretation
- Better message for closed empty elements
- Rename `Error::ClosedEmptyElement` -> `Error::ClosedVoidElement`
- Missed old ClosedEmptyElement
- Bad space padding in error messages
- Use `IndexMap` or `HashMap` instead of some `BTreeMap`s
- Nested `if`s and `for`s in `pochoir-template-engine` and `pochoir-compiler`
- Spans for statement errors
- Context::from_iter not having the default context
- Avoid panicking when failing to evaluate an expression
- `take` and `skip` functions panicking when offset is greater than array length
- Bad error span when wrapping errors from `parser-common`
- Enforce variable name characters in let statements which is useful to provide a better error message when `=` is forgotten
- Enforce variable name characters in for statements
- Avoid exiting on error when watching files in build mode
- Better close tag name parsing
- Better error when forgetting closing ">"
- Support avoiding space before `/` in self-closing tags
- Support non-quoted attributes without spaces before `/` in self-closing elements
- Better field name parsing in objects
- Finally parse correctly closing tags and display errors with the right row/column
- Better parsing of attribute without quotes followed by "/>"
- Better error messages for starting and closing tags without name
- Better error for close tag with spaces before name
- Support space and newlines in indexes
- Throw error when using unbounded ranges in for loops
- Source of errors for unbounded range error
- Avoid panicking when an invalid CSS selector is used in `Tree::{select,select_all}` and `TreeRef::select_all`
- Avoid highlighting spaces in errors
- Strange hardcoded type in error messages
- Not consistent newlines
- `SpannedWithComponent` should be part of the public API
- Improve polymorphism by creating inner functions
- Avoid panicking when indexing a string or array by a range outside bounds
- Avoid cloning arguments given to functions
- Rename `to_value` -> `serialize_to_value`, `from_value` -> `deserialize_from_value`
- Forgot comma
- Improve some errors
- Improve error spans to take account of file offsets
- Bad serde representation of `Object`s
- Improve live reloading
- Emit events `BeforeElement` and `AfterElement` in case of empty elements
- Avoid cloning the global context
- Implement or remove a lot of TODOs

### Documentation

- Add documentation and forbid unsafe code
- Add some documentation about namespaced functions (and add another test)
- Update description
- Better explanation of `Null` type
- Add missing backtick
- Prettify functions examples and tables of object fields
- Fix links
- Fix docs
- Fix code block of `sort` function
- Add static map provider example
- Add filesystem provider example
- Update readme
- Write the main compiler documentation
- Fix all documentation warnings
- Improve docs
- Add documentation to some transformer methods
- Improve the documentation of the `FromValue` and `IntoValue` traits

### Features

- Start StreamParser implementation + rename StoredFunction to Function + rename to_stored_function to Function::from
- First template engine implementation
- Continue improving the codebase: add error type, support namespaced functions (and rename Function::from to Function::new)
- Support basic tests + rewrite using StreamParser + add Escape enum in pochoir-common
- Support all tests + remove StreamParser::catch_up + add StreamParser::set_index
- Refactor the parser using the new StreamParser structure and support new lines in attribute values
- Add Owned version of the Tree
- Add a function to render a Tree to an HTML string
- Start rewriting the compiler
- Add `to_number` function
- Lazy evaluate conditions
- Write a basic interpreter
- Use Spanned structure to store span of each parsing node in pochoir-parser and pochoir-template-engine
- Support preserving type of expressions in attributes, support statements in attributes
- Store the span of all errors using the Spanned structure
- Return an error when an unknown statement is encountered
- Implement Clone for all error types
- Add support for <template> syntax
- Add a cli
- Better errors (remove Span structure), fix asset management
- Enable use of owned strings in `TreeRefMut::{append_html,prepend_html,replace_node}` by adding `deep_clone` to various structures
- Store name of the file producing the `Error` in the `Spanned` structure
- Display beautiful error messages in `pochoir-cli`, fix start span of `pochoir-lang` errors, add a function in `pochoir_common::Spanned` to get row/column position
- Support definition operator
- Change behavior for inherited variables (`globals` before): make the `Context::insert_inherited` and `Context::make_inherited` methods and use them in `pochoir-compiler`
- Supporting outputting the error using ANSI escape codes
- Make some more fine-tuned errors from low level errors happening in the stream parser
- Move functions to display errors into `pochoir-common`
- Add `let` statement in `pochoir_compiler` and `pochoir_template_engine`
- Support optional arguments
- Add a bunch of default functions: skip, split, take
- Add optional parameter to `enumerate` to set the start index, remove `enumerate_from``
- Support destructuring values in for loops in `pochoir-compiler` and `pochoir-template-engine`
- Better errors: make some inner errors transparent
- Better errors for unclosed `(` in functions, grouping context, and `[` when indexing
- Implement `iter` function
- Add `entries` function
- Add the `first` and `last` functions
- Support spaces and new lines between `(` and `)` in function calls
- Add `FunctionError` and `FunctionResult` helper types
- Force custom components to follow web-component custom element name convention
- Add `trim` function
- Support multiplying strings
- Convert case of component properties
- Support again ASCII whitespace in tag names
- Support ranges
- Use u32 as start and end index in ranges
- Improve error spans in some binary operators
- Add two providers, store component name in SpannedWithComponent
- Add `sort` and `sort_by` functions
- Take mutable reference of tree in `on_after_element` event handler
- Implement `IntoIterator` for `Context`
- Implement `Extend` for `Context`
- Add method to `Tree` to get all nodes
- Implement CSS enhancer supporting scoped CSS
- Support deeply nested functions
- Implement `IntoValue` for Context
- Introduce a new `Object` structure with an `object!` macro for easy creation
- Add `IntoValue` derive macro
- Add implementation of FromValue/IntoValue for HashMap and BTreeMap
- Implement Serialize + Deserialize for Object
- Add IntoValue macro support for all kinds of structures and enumerations
- Implement AccessibilityChecker
- Add argument to change development server port
- Add a `round` function
- Don't ignore template blocks when rendering tree
- Add a `replace` function
- Support `elif` statements
- Assignments return the assigned value
- Add support for the `if let` syntax to check if an expression equals to `null`
- Implement `IntoValue` for `[T; N]`
- Add `Value::{index, index_mut}` methods to easily index arrays
- Add `Context::from_object`
- Add `Value::{as_*}` in case pattern matching should be avoided
- Implement `FromValue` derive macro
- Support negative ranges
- Make `is_custom_element` public
- Introduce transformers
- Add some functions: `starts_with`, `ends_with` and `capitalize`
- Add the `TreeRef::closest` method
- Implement error handling in transformers

### Miscellaneous Tasks

- Use larger version dependency for `proc-macro2`
- Rename examples
- Fix some warnings
- Fix some warnings in release mode
- Add more cargo lints and fix warnings
- Fix all Clippy warnings and improve documentation

### Refactor

- Change architecture of crates + support functions in Context of pochoir-common + `StoredFunction` is wrapped in Arc
- Support template syntax in the parser
- Implement templating in pochoir-compiler: support scoped slots
- Use Spanned
- Use the `UnaryOp`, `BinaryOp` and `TernaryOp` structures instead of a single `Operator` structure
- Rename `Error::ParserError` -> `Error::StreamParserError` where possible
- Rename `pochoir-compiler` crate -> `pochoir` and export all other sibling crates from it
- Move some structures around
- Improve code quality

### Styling

- Run `cargo fmt`

### Testing

- Add test for nested if
- Test for boolean without expression in if statements
- Add a test for each default function + fix display of objects in `Value`
- Add tests for object and array with newlines
- Fix tests of `TreeRef::text`

### Refator

- Add some public exports in the main `pochoir` crate

## [0.8.0] - 2023-05-19

### Bug Fixes

- `word_count` function counting extra spaces as words
- Avoid panicking due to the root node being reached when using `select`
- Avoid including new lines in an element name
- Slots with sync version of `parser_common`
- Unescaped expressions in attributes not working
- [**breaking**] Consume the `TreeRefMut` in methods that remove the referenced node
- Bad index computation when replacing, prepending or appending
- All problems with IDs when replacing, appending or prepending nodes
- Make all pochoir functions Send + Sync
- Bad error span when using doctypes (found when fuzzing)

### Documentation

- Remove an old documentation item
- Change list style
- Improve documentation of the `eval` module
- Better list style
- Add JSON example in `Context::from_serialize`
- Fix documentation links
- Fix code example

### Features

- Make `TreeRef::is_matching` API public
- Add a `TreeRef::sub_tree` method to make a tree from the children of a `TreeRef`
- [**breaking**] Add an argument to `TreeRefMut::set_attr` to escape the attribute value
- [**breaking**] Store boxed functions instead of pointers
- Add `Value::{get, get_mut}` functions to get the value of an object field
- Use real Rust functions as pochoir functions
- Make `TemplateCompiler` clonable

### Refactor

- [**breaking**] Use a custom Value type
- [**breaking**] Remove `TreeRef::from_id`, add `TreeRefId::get` to replace it, rename `TreeRef::get` and `TreeRef::get_mut` to `TreeRef::inner` and `TreeRef::inner_mut`
- [**breaking**] Remove Node::prev_sibling and Node::next_sibling, use TreeRef to search in the children of the node parent instead
- [**breaking**] Avoid storing nodes in tree using a Rc<RefCell<T>>
- [**breaking**] Swap lifetimes of `TreeRef` and `TreeRefMut`
- [**breaking**] Create a better API for `Tree`s
- [**breaking**] Take `Into<AttrKey>` in `Attrs::{contains_key, get, get_key_value, get_mut, insert, remove}`, `Attrs::insert` takes directly a key/value pair without their span but `Attrs::insert_with_span` can be used instead
- [**breaking**] `Tree::{get, get_mut}` panic if the ID is not found, add `Tree::{try_get, try_get_mut}` for non-panicking versions
- Split tree management code
- [**breaking**] Accept `Into<Cow<str>>` in `Context::{insert, remove, contains_key, get, get_mut}`

### Styling

- Run rustfmt

### Testing

- Add tests for `Tree`

### Build

- Use workspace dependencies

## [0.7.0] - 2023-04-21

### Bug Fixes

- Use the correct component name
- Default slot content not working with named slots

### Documentation

- Add documentation for scoped slots
- Add `enumerate` function in the list

### Features

- Support nesting if and for statements
- [**breaking**] Use `<slot></slot>` syntax to insert slots and support default content
- [**breaking**] Add support for named slots using the syntax of Web Components (`<slot name="">` and `slot` attribute)
- Add initial support for scoped slots (+ avoid using the Context to pass slots)
- Support nested slots
- [**breaking**] Replace JSON support in attributes by pochoir's custom-language expressions

### Refactor

- Use a custom Debug implementation for the `TreeRef` structure

### Testing

- Add test for multiple instances of a slot with the same name
- Add test to use a component as a slot
- Add test for nested if statements in for statements

## [0.6.0] - 2023-04-19

### Bug Fixes

- "end tag has no corresponding start tag" span
- Wrong number rendering
- Wrong utf-8 offsets in `ParseStream::take_while_peekable`

### Documentation

- Add examples of each function
- Fix some examples
- Fix documentation link

### Features

- Add an `enumerate` function
- [**breaking**] Add an optional parameter to the `enumerate` function to change the start index with 1 as default value

### Miscellaneous Tasks

- Update `git-cliff` configuration

### Build

- Update dependencies

## [0.5.0] - 2023-02-01

### Bug Fixes

- Bad `serde` features in release mode
- Strange `start_offset` computations in `ParseStream`
- Use manual error formatting when an error happens during template parsing
- Bad return lifetimes of `TreeRef::{select,select_all,name,text,attr}`

### Documentation

- Fix the documentation for the `TemplateCompiler` structure
- Fix example

### Features

- Implement `FromIterator` for `Context`
- Support comments
- Add a `Context::from_string` method to make a `Context` from a string
- Add a mini-language in expressions, fix error spans

### Miscellaneous Tasks

- Add a benchmark with Criterion
- Set up fuzzing with `cargo-fuzz`
- Update copyright year
- Add a CHANGELOG generated with git-cliff

### Refactor

- Simplify the `TemplateCompiler::render_error` method

## [0.4.5] - 2022-11-27

### Bug Fixes

- Parse children as raw text in <script>, <style>, <noscript> and <textarea>

## [0.4.4] - 2022-11-26

### Bug Fixes

- Prevent panicking when removing a child of the root node

## [0.4.3] - 2022-11-20

### Bug Fixes

- Bad parent ID in `TreeRef::{replace_node, append_children, prepend_children}`

## [0.4.2] - 2022-11-20

### Bug Fixes

- Support `Null` values in `Context::from_serialize`
- [**breaking**] Take an `IntoIterator` in `Tree::append_nodes` + take `Tree` by reference in `TreeRef::{replace_node, append_node, prepend_node}` + fix some Clippy warnings + run `cargo fmt`

### Features

- Implement `std::error::Error` for the custom `Error` type

### Refactor

- [**breaking**] Remove the lifetime of `Error`, the lifetime `'name` of `ParseStream` and the lifetime `'name` of `TemplateCompiler`
- [**breaking**] Pass the parser as type parameter, remove the `self` argument in the `Parser::parse`method

## [0.4.1] - 2022-09-04

### Bug Fixes

- Bad IDs returned by `Tree::next_id`

### Features

- Support global variables
- Add a `TemplateCompiled::diff` method

## [0.4.0] - 2022-08-28

### Bug Fixes

- Some closed `if`s were found unclosed

### Features

- Add a `sync` feature to implement `Send + Sync` for the `Tree` structure
- Support JSON in attributes
- Escape content passed to `TreeRef::set_text`

## [0.3.0] - 2022-08-26

### Bug Fixes

- Don't require the `data` lifetime when using the `TemplateCompiler::compile` method
- Store attribute span + improve error message location

### Documentation

- Fix the code example

### Features

- Support escaping expressions
- Implement `Send + Sync` for `TemplateCompiler`
- Use a `Cow<str>` to represent names
- Implement `Display` for `Context`
- Support filters

### Styling

- Run `cargo fmt`

## [0.2.0] - 2022-08-25

### Bug Fixes

- `TreeRef::append_node` and `TreeRef::prepend_node` manipulate children instead of siblings

### Features

- [**breaking**] Various API improvements
- [**breaking**] Major architecture shift bringing a lot of API improvements

### Miscellaneous Tasks

- Update `cssparser`

## [0.1.0] - 2022-08-19

### Miscellaneous Tasks

- Initial commit

<!-- generated by git-cliff -->
